;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-keysym-scancode-raw  sdl-keysym-scancode-raw-set!
        sdl-keysym-sym-raw       sdl-keysym-sym-raw-set!
        sdl-keysym-mod-raw       sdl-keysym-mod-raw-set!

        sdl-keysym-scancode      sdl-keysym-scancode-set!
        sdl-keysym-sym           sdl-keysym-sym-set!
        sdl-keysym-mod           sdl-keysym-mod-set!)


(define-struct-field-accessors
  SDL_Keysym*
  sdl-keysym?
  ("scancode"
   type:   SDL_Scancode
   getter: sdl-keysym-scancode-raw
   setter: sdl-keysym-scancode-raw-set!
   guard:  (int-guard "sdl-keysym field scancode"))
  ("sym"
   type:   SDL_Keycode
   getter: sdl-keysym-sym-raw
   setter: sdl-keysym-sym-raw-set!
   guard:  (int-guard "sdl-keysym field sym"))
  ("mod"
   type:   Uint16
   getter: sdl-keysym-mod-raw
   setter: sdl-keysym-mod-raw-set!
   guard:  (Uint16-guard "sdl-keysym field mod")))


(define-enum-accessor
  getter: (sdl-keysym-scancode
           raw:   sdl-keysym-scancode-raw
           conv:  sdl-scancode->symbol)
  setter: (sdl-keysym-scancode-set!
           raw:   sdl-keysym-scancode-raw-set!
           conv:  sdl-symbol->scancode))


(define-enum-accessor
  getter: (sdl-keysym-sym
           raw:   sdl-keysym-sym-raw
           conv:  sdl-keycode->symbol)
  setter: (sdl-keysym-sym-set!
           raw:   sdl-keysym-sym-raw-set!
           conv:  sdl-symbol->keycode))


(define-enum-mask-accessor
  getter: (sdl-keysym-mod
           raw:    sdl-keysym-mod-raw
           unpack: sdl-unpack-keymods
           exact:  #f)
  setter: (sdl-keysym-mod-set!
           raw:    sdl-keysym-mod-raw-set!
           pack:   sdl-pack-keymods))
