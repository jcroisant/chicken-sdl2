;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-pixel-format-format-raw
        sdl-pixel-format-format
        sdl-pixel-format-palette  sdl-pixel-format-palette-set!
        sdl-pixel-format-bits-per-pixel
        sdl-pixel-format-bytes-per-pixel
        sdl-pixel-format-rmask
        sdl-pixel-format-gmask
        sdl-pixel-format-bmask
        sdl-pixel-format-amask)

(define-struct-field-accessors
  SDL_PixelFormat*
  sdl-pixel-format?
  ("format"
   type:   SDL_PixelFormatEnum
   getter: sdl-pixel-format-format-raw)
  ;; See below.
  ;; ("palette"
  ;;  type:   SDL_Palette*
  ;;  getter: sdl-pixel-format-palette)
  ("BitsPerPixel"
   type: Uint8
   getter: sdl-pixel-format-bits-per-pixel)
  ("BytesPerPixel"
   type: Uint8
   getter: sdl-pixel-format-bytes-per-pixel)
  ("Rmask"
   type: Uint32
   getter: sdl-pixel-format-rmask)
  ("Gmask"
   type: Uint32
   getter: sdl-pixel-format-gmask)
  ("Bmask"
   type: Uint32
   getter: sdl-pixel-format-bmask)
  ("Amask"
   type: Uint32
   getter: sdl-pixel-format-amask)
  ;; omitted: Rloss    (internal use)
  ;; omitted: Gloss    (internal use)
  ;; omitted: Bloss    (internal use)
  ;; omitted: Aloss    (internal use)
  ;; omitted: Rshift   (internal use)
  ;; omitted: Gshift   (internal use)
  ;; omitted: Bshift   (internal use)
  ;; omitted: Ashift   (internal use)
  ;; omitted: refcount (internal use)
  ;; omitted: next     (internal use)
  )


(define-enum-accessor
  getter: (sdl-pixel-format-format
           raw:   sdl-pixel-format-format-raw
           conv:  sdl-pixel-format-enum->symbol))


(define (sdl-pixel-format-palette-set! pixel-format palette)
  (SDL_SetPixelFormatPalette pixel-format palette))

(define sdl-pixel-format-palette
  (struct-field-getter SDL_PixelFormat*
                       sdl-pixel-format?
                       SDL_Palette*
                       "palette"))

(set! (setter sdl-pixel-format-palette)
      sdl-pixel-format-palette-set!)
