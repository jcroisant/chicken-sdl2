;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-audio-spec-freq      sdl-audio-spec-freq-set!
        sdl-audio-spec-format    sdl-audio-spec-format-set!
        sdl-audio-spec-channels  sdl-audio-spec-channels-set!
        sdl-audio-spec-silence
        sdl-audio-spec-samples   sdl-audio-spec-samples-set!
        sdl-audio-spec-size
        sdl-audio-spec-callback  sdl-audio-spec-callback-set!
        sdl-audio-spec-userdata  sdl-audio-spec-userdata-set!)


(define-struct-field-accessors
  SDL_AudioSpec*
  sdl-audio-spec?
  ("freq"
   type:   int
   getter: sdl-audio-spec-freq
   setter: sdl-audio-spec-freq-set!
   guard:  (int-guard "sdl-audio-spec field freq"))
  ("format"
   type:   SDL_AudioFormat
   getter: sdl-audio-spec-format
   setter: sdl-audio-spec-format-set!
   guard:  noop-guard)
  ("channels"
   type:   Uint8
   getter: sdl-audio-spec-channels
   setter: sdl-audio-spec-channels-set!
   guard:  (Uint8-guard "sdl-audio-spec field channels"))
  ("silence"
   type:   Uint8
   getter: sdl-audio-spec-silence
   ;; no setter because value is calculated by SDL_OpenAudioDevice
   )
  ("samples"
   type:   Uint16
   getter: sdl-audio-spec-samples
   setter: sdl-audio-spec-samples-set!
   guard:  (Uint16-guard "sdl-audio-spec field samples"))
  ("size"
   type:   Uint32
   getter: sdl-audio-spec-size
   ;; no setter because value is calculated by SDL_OpenAudioDevice
   )
  ("callback"
   type:   SDL_AudioCallback
   getter: sdl-audio-spec-callback
   setter: sdl-audio-spec-callback-set!
   guard:  noop-guard)
  ("userdata"
   type:   c-pointer
   getter: sdl-audio-spec-userdata
   setter: sdl-audio-spec-userdata-set!
   guard:  noop-guard))
