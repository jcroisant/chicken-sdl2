;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-multi-gesture-event?
        sdl-multi-gesture-event-touch-id
        sdl-multi-gesture-event-touch-id-set!
        sdl-multi-gesture-event-dtheta
        sdl-multi-gesture-event-dtheta-set!
        sdl-multi-gesture-event-ddist
        sdl-multi-gesture-event-ddist-set!
        sdl-multi-gesture-event-x
        sdl-multi-gesture-event-x-set!
        sdl-multi-gesture-event-y
        sdl-multi-gesture-event-y-set!
        sdl-multi-gesture-event-num-fingers
        sdl-multi-gesture-event-num-fingers-set!)


(define-sdl-event-type "SDL_MultiGestureEvent"
  types: (SDL_MULTIGESTURE)
  pred:  sdl-multi-gesture-event?
  print: ((touch-id sdl-multi-gesture-event-touch-id)
          (dtheta sdl-multi-gesture-event-dtheta)
          (ddist sdl-multi-gesture-event-ddist)
          (x sdl-multi-gesture-event-x)
          (y sdl-multi-gesture-event-y)
          (num-fingers sdl-multi-gesture-event-num-fingers))
  ("mgesture.touchId"
   type:   SDL_TouchID
   getter: sdl-multi-gesture-event-touch-id
   setter: sdl-multi-gesture-event-touch-id-set!
   guard:  noop-guard)
  ("mgesture.dTheta"
   type:   float
   getter: sdl-multi-gesture-event-dtheta
   setter: sdl-multi-gesture-event-dtheta-set!
   guard:  noop-guard)
  ("mgesture.dDist"
   type:   float
   getter: sdl-multi-gesture-event-ddist
   setter: sdl-multi-gesture-event-ddist-set!
   guard:  noop-guard)
  ("mgesture.x"
   type:   float
   getter: sdl-multi-gesture-event-x
   setter: sdl-multi-gesture-event-x-set!
   guard:  noop-guard)
  ("mgesture.y"
   type:   float
   getter: sdl-multi-gesture-event-y
   setter: sdl-multi-gesture-event-y-set!
   guard:  noop-guard)
  ("mgesture.numFingers"
   type:   Uint16
   getter: sdl-multi-gesture-event-num-fingers
   setter: sdl-multi-gesture-event-num-fingers-set!
   guard:  (Uint16-guard "sdl-multi-gesture-event field numFingers")))
