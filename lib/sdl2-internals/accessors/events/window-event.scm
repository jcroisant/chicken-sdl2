;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-window-event?
        sdl-window-event-window-id
        sdl-window-event-window-id-set!
        sdl-window-event-event-raw
        sdl-window-event-event-raw-set!
        sdl-window-event-event
        sdl-window-event-event-set!
        sdl-window-event-data1
        sdl-window-event-data1-set!
        sdl-window-event-data2
        sdl-window-event-data2-set!)


(define-sdl-event-type "SDL_WindowEvent"
  types: (SDL_WINDOWEVENT)
  pred:  sdl-window-event?
  print: ((window-id sdl-window-event-window-id)
          (event sdl-window-event-event)
          (data1 sdl-window-event-data1)
          (data2 sdl-window-event-data2))
  ("window.windowID"
   type:   Uint32
   getter: sdl-window-event-window-id
   setter: sdl-window-event-window-id-set!
   guard:  (Uint32-guard "sdl-window-event field windowID"))
  ("window.event"
   type:   Uint8
   getter: sdl-window-event-event-raw
   setter: sdl-window-event-event-raw-set!
   guard:  (Uint8-guard "sdl-window-event field event"))
  ("window.data1"
   type:   Sint32
   getter: sdl-window-event-data1
   setter: sdl-window-event-data1-set!
   guard:  (Sint32-guard "sdl-window-event field data1"))
  ("window.data2"
   type:   Sint32
   getter: sdl-window-event-data2
   setter: sdl-window-event-data2-set!
   guard:  (Sint32-guard "sdl-window-event field data2")))


(define-enum-accessor
  getter: (sdl-window-event-event
           raw:  sdl-window-event-event-raw
           conv: sdl-window-event-id->symbol)
  setter: (sdl-window-event-event-set!
           raw:  sdl-window-event-event-raw-set!
           conv: sdl-symbol->window-event-id))
