;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-mouse-button-event?
        sdl-mouse-button-event-window-id
        sdl-mouse-button-event-window-id-set!
        sdl-mouse-button-event-which
        sdl-mouse-button-event-which-set!
        sdl-mouse-button-event-button-raw
        sdl-mouse-button-event-button-raw-set!
        sdl-mouse-button-event-button
        sdl-mouse-button-event-button-set!
        sdl-mouse-button-event-state
        sdl-mouse-button-event-state-set!
        sdl-mouse-button-event-x
        sdl-mouse-button-event-x-set!
        sdl-mouse-button-event-y
        sdl-mouse-button-event-y-set!)


(define-sdl-event-type "SDL_MouseButtonEvent"
  types: (SDL_MOUSEBUTTONDOWN
          SDL_MOUSEBUTTONUP)
  pred:  sdl-mouse-button-event?
  print: ((button sdl-mouse-button-event-button)
          (x sdl-mouse-button-event-x)
          (y sdl-mouse-button-event-y))
  ("button.windowID"
   type:   Uint32
   getter: sdl-mouse-button-event-window-id
   setter: sdl-mouse-button-event-window-id-set!
   guard:  (Uint32-guard "sdl-mouse-button-event field windowID"))
  ("button.which"
   type:   Uint32
   getter: sdl-mouse-button-event-which
   setter: sdl-mouse-button-event-which-set!
   guard:  (Uint32-guard "sdl-mouse-button-event field which"))
  ("button.button"
   type:   Uint8
   getter: sdl-mouse-button-event-button-raw
   setter: sdl-mouse-button-event-button-raw-set!
   guard:  (Uint8-guard "sdl-mouse-button-event field button"))
  ("button.state"
   type:   bool
   getter: sdl-mouse-button-event-state
   setter: sdl-mouse-button-event-state-set!
   guard:  noop-guard)
  ("button.x"
   type:   Sint32
   getter: sdl-mouse-button-event-x
   setter: sdl-mouse-button-event-x-set!
   guard:  (Sint32-guard "sdl-mouse-button-event field x"))
  ("button.y"
   type:   Sint32
   getter: sdl-mouse-button-event-y
   setter: sdl-mouse-button-event-y-set!
   guard:  (Sint32-guard "sdl-mouse-button-event field y")))


(define-enum-accessor
  getter: (sdl-mouse-button-event-button
           raw:  sdl-mouse-button-event-button-raw
           conv: sdl-mouse-button->symbol)
  setter: (sdl-mouse-button-event-button-set!
           raw:  sdl-mouse-button-event-button-raw-set!
           conv: sdl-symbol->mouse-button))
