;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-dollar-gesture-event?
        sdl-dollar-gesture-event-touch-id
        sdl-dollar-gesture-event-touch-id-set!
        sdl-dollar-gesture-event-gesture-id
        sdl-dollar-gesture-event-gesture-id-set!
        sdl-dollar-gesture-event-num-fingers
        sdl-dollar-gesture-event-num-fingers-set!
        sdl-dollar-gesture-event-error
        sdl-dollar-gesture-event-error-set!
        sdl-dollar-gesture-event-x
        sdl-dollar-gesture-event-x-set!
        sdl-dollar-gesture-event-y
        sdl-dollar-gesture-event-y-set!)


(define-sdl-event-type "SDL_DollarGestureEvent"
  types: (SDL_DOLLARGESTURE
          SDL_DOLLARRECORD)
  pred:  sdl-dollar-gesture-event?
  print: ((touch-id sdl-dollar-gesture-event-touch-id)
          (gesture-id sdl-dollar-gesture-event-gesture-id)
          (num-fingers sdl-dollar-gesture-event-num-fingers)
          (error sdl-dollar-gesture-event-error)
          (x sdl-dollar-gesture-event-x)
          (y sdl-dollar-gesture-event-y))
  ("dgesture.touchId"
   type:   SDL_TouchID
   getter: sdl-dollar-gesture-event-touch-id
   setter: sdl-dollar-gesture-event-touch-id-set!
   guard:  noop-guard)
  ("dgesture.gestureId"
   type:   SDL_GestureID
   getter: sdl-dollar-gesture-event-gesture-id
   setter: sdl-dollar-gesture-event-gesture-id-set!
   guard:  noop-guard)
  ("dgesture.numFingers"
   type:   Uint32
   getter: sdl-dollar-gesture-event-num-fingers
   setter: sdl-dollar-gesture-event-num-fingers-set!
   guard:  (Uint32-guard "sdl-dollar-gesture-event field numFingers"))
  ("dgesture.error"
   type:   float
   getter: sdl-dollar-gesture-event-error
   setter: sdl-dollar-gesture-event-error-set!
   guard:  noop-guard)
  ("dgesture.x"
   type:   float
   getter: sdl-dollar-gesture-event-x
   setter: sdl-dollar-gesture-event-x-set!
   guard:  noop-guard)
  ("dgesture.y"
   type:   float
   getter: sdl-dollar-gesture-event-y
   setter: sdl-dollar-gesture-event-y-set!
   guard:  noop-guard))
