;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-touch-finger-event?
        sdl-touch-finger-event-touch-id
        sdl-touch-finger-event-touch-id-set!
        sdl-touch-finger-event-finger-id
        sdl-touch-finger-event-finger-id-set!
        sdl-touch-finger-event-x
        sdl-touch-finger-event-x-set!
        sdl-touch-finger-event-y
        sdl-touch-finger-event-y-set!
        sdl-touch-finger-event-dx
        sdl-touch-finger-event-dx-set!
        sdl-touch-finger-event-dy
        sdl-touch-finger-event-dy-set!
        sdl-touch-finger-event-pressure
        sdl-touch-finger-event-pressure-set!)


(define-sdl-event-type "SDL_TouchFingerEvent"
  types: (SDL_FINGERDOWN
          SDL_FINGERUP
          SDL_FINGERMOTION)
  pred:  sdl-touch-finger-event?
  print: ((touch-id sdl-touch-finger-event-touch-id)
          (finger-id sdl-touch-finger-event-finger-id)
          (x sdl-touch-finger-event-x)
          (y sdl-touch-finger-event-y)
          (dx sdl-touch-finger-event-dx)
          (dy sdl-touch-finger-event-dy)
          (pressure sdl-touch-finger-event-pressure))
  ("tfinger.touchId"
   type:   SDL_TouchID
   getter: sdl-touch-finger-event-touch-id
   setter: sdl-touch-finger-event-touch-id-set!
   guard:  noop-guard)
  ("tfinger.fingerId"
   type:   SDL_FingerID
   getter: sdl-touch-finger-event-finger-id
   setter: sdl-touch-finger-event-finger-id-set!
   guard:  noop-guard)
  ("tfinger.x"
   type:   float
   getter: sdl-touch-finger-event-x
   setter: sdl-touch-finger-event-x-set!
   guard:  noop-guard)
  ("tfinger.y"
   type:   float
   getter: sdl-touch-finger-event-y
   setter: sdl-touch-finger-event-y-set!
   guard:  noop-guard)
  ("tfinger.dx"
   type:   float
   getter: sdl-touch-finger-event-dx
   setter: sdl-touch-finger-event-dx-set!
   guard:  noop-guard)
  ("tfinger.dy"
   type:   float
   getter: sdl-touch-finger-event-dy
   setter: sdl-touch-finger-event-dy-set!
   guard:  noop-guard)
  ("tfinger.pressure"
   type:   float
   getter: sdl-touch-finger-event-pressure
   setter: sdl-touch-finger-event-pressure-set!
   guard:  noop-guard))
