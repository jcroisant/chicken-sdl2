;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-user-event?
        sdl-user-event-window-id
        sdl-user-event-window-id-set!
        sdl-user-event-code
        sdl-user-event-code-set!
        sdl-user-event-data1
        sdl-user-event-data1-set!
        sdl-user-event-data2
        sdl-user-event-data2-set!)


(define-sdl-event-type "SDL_UserEvent"
  types: (SDL_USEREVENT)
  pred:  sdl-user-event?
  print: ((code sdl-user-event-code))
  ("user.windowID"
   type:   Uint32
   getter: sdl-user-event-window-id
   setter: sdl-user-event-window-id-set!
   guard:  (Uint32-guard "sdl-user-event field windowID"))
  ("user.code"
   type:   Sint32
   getter: sdl-user-event-code
   setter: sdl-user-event-code-set!
   guard:  (Sint32-guard "sdl-user-event field code"))
  ("user.data1"
   type:   c-pointer
   getter: sdl-user-event-data1
   setter: sdl-user-event-data1-set!
   guard:  noop-guard)
  ("user.data2"
   type:   c-pointer
   getter: sdl-user-event-data2
   setter: sdl-user-event-data2-set!
   guard:  noop-guard))
