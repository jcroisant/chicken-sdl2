;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-joy-hat-event?
        sdl-joy-hat-event-which
        sdl-joy-hat-event-which-set!
        sdl-joy-hat-event-hat
        sdl-joy-hat-event-hat-set!
        sdl-joy-hat-event-value-raw
        sdl-joy-hat-event-value-raw-set!
        sdl-joy-hat-event-value
        sdl-joy-hat-event-value-set!)


(define-sdl-event-type "SDL_JoyHatEvent"
  types: (SDL_JOYHATMOTION)
  pred:  sdl-joy-hat-event?
  print: ((which sdl-joy-hat-event-which)
          (hat sdl-joy-hat-event-hat)
          (value sdl-joy-hat-event-value))
  ("jhat.which"
   type:   SDL_JoystickID
   getter: sdl-joy-hat-event-which
   setter: sdl-joy-hat-event-which-set!
   guard:  noop-guard)
  ("jhat.hat"
   type:   Uint8
   getter: sdl-joy-hat-event-hat
   setter: sdl-joy-hat-event-hat-set!
   guard:  (Uint8-guard "sdl-joy-hat-event field hat"))
  ("jhat.value"
   type:   Uint8
   getter: sdl-joy-hat-event-value-raw
   setter: sdl-joy-hat-event-value-raw-set!
   guard:  (Uint8-guard "sdl-joy-hat-event field value")))


(define-enum-accessor
  getter: (sdl-joy-hat-event-value
           raw:  sdl-joy-hat-event-value-raw
           conv: sdl-joystick-hat-position->symbol)
  setter: (sdl-joy-hat-event-value-set!
           raw:  sdl-joy-hat-event-value-raw-set!
           conv: sdl-symbol->joystick-hat-position))
