;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-keyboard-event?
        sdl-keyboard-event-window-id
        sdl-keyboard-event-window-id-set!
        sdl-keyboard-event-state
        sdl-keyboard-event-state-set!
        sdl-keyboard-event-repeat
        sdl-keyboard-event-repeat-set!
        sdl-keyboard-event-keysym
        sdl-keyboard-event-keysym-set!

        ;; Shortcuts
        sdl-keyboard-event-sym-raw       sdl-keyboard-event-sym-raw-set!
        sdl-keyboard-event-sym           sdl-keyboard-event-sym-set!
        sdl-keyboard-event-scancode-raw  sdl-keyboard-event-scancode-raw-set!
        sdl-keyboard-event-scancode      sdl-keyboard-event-scancode-set!
        sdl-keyboard-event-mod-raw       sdl-keyboard-event-mod-raw-set!
        sdl-keyboard-event-mod           sdl-keyboard-event-mod-set!)


(define-sdl-event-type "SDL_KeyboardEvent"
  types: (SDL_KEYDOWN
          SDL_KEYUP)
  pred:  sdl-keyboard-event?
  print: ((sym sdl-keyboard-event-sym)
          (scancode sdl-keyboard-event-scancode)
          (mod sdl-keyboard-event-mod))
  ("key.windowID"
   type:   Uint32
   getter: sdl-keyboard-event-window-id
   setter: sdl-keyboard-event-window-id-set!
   guard:  (Uint32-guard "sdl-keyboard-event field windowID"))
  ("key.state"
   type:   bool
   getter: sdl-keyboard-event-state
   setter: sdl-keyboard-event-state-set!
   guard:  noop-guard)
  ("key.repeat"
   type:   Uint8
   getter: sdl-keyboard-event-repeat
   setter: sdl-keyboard-event-repeat-set!
   guard:  (Uint8-guard "sdl-keyboard-event field repeat"))
  ;; See below
  ;; ("key.keysym"
  ;;  type:   SDL_Keysym
  ;;  getter: sdl-keyboard-event-keysym
  ;;  setter: sdl-keyboard-event-keysym-set!
  ;;  guard:  noop-guard)
  )


;;; Since the keysym is stored in the event struct by value, not a
;;; pointer, we need to treat it specially. In particular, we don't
;;; access the event's keysym directly, but rather copy its value
;;; to/from a keysym being held separately. It would not be safe to
;;; wrap a pointer to the event's keysym, because the event could be
;;; freed/GC'd while there is still a Scheme reference to the keysym.

(define (sdl-keyboard-event-keysym-set! event keysym)
  (define foreign-setter
    ;; Copy the given keysym's value into the event's keysym.
    (foreign-lambda*
     void ((SDL_Event* event) (SDL_Keysym* keysym))
     "event->key.keysym = *keysym;"))
  (assert (sdl-keyboard-event? event))
  (foreign-setter event keysym))

(define (sdl-keyboard-event-keysym event)
  (define foreign-getter
    ;; Allocate a new keysym and then copy the event's keysym value.
    (foreign-lambda*
     void ((SDL_Event* event) (SDL_Keysym* keysym))
     "*keysym = event->key.keysym;"))
  (assert (sdl-keyboard-event? event))
  (let ((keysym (sdl-alloc-keysym)))
    (foreign-getter event keysym)
    keysym))

(set! (setter sdl-keyboard-event-keysym)
      sdl-keyboard-event-keysym-set!)



;;; Shortcuts for directly getting values out of the event keysym.
;;; These are more user-convenient (less code to write) and efficient
;;; (less overhead from allocating/copying memory).

(define-struct-field-accessors
  SDL_Event*
  sdl-keyboard-event?
  ("key.keysym.scancode"
   type:   SDL_Scancode
   getter: sdl-keyboard-event-scancode-raw
   setter: sdl-keyboard-event-scancode-raw-set!
   guard:  (int-guard "sdl-keyboard-event field scancode"))
  ("key.keysym.sym"
   type:   SDL_Keycode
   getter: sdl-keyboard-event-sym-raw
   setter: sdl-keyboard-event-sym-raw-set!
   guard:  (int-guard "sdl-keyboard-event field sym"))
  ("key.keysym.mod"
   type:   Uint16
   getter: sdl-keyboard-event-mod-raw
   setter: sdl-keyboard-event-mod-raw-set!
   guard:  (Uint16-guard "sdl-keyboard-event field mod")))


(define-enum-accessor
  getter: (sdl-keyboard-event-scancode
           raw:   sdl-keyboard-event-scancode-raw
           conv:  sdl-scancode->symbol)
  setter: (sdl-keyboard-event-scancode-set!
           raw:   sdl-keyboard-event-scancode-raw-set!
           conv:  sdl-symbol->scancode))

(define-enum-accessor
  getter: (sdl-keyboard-event-sym
           raw:   sdl-keyboard-event-sym-raw
           conv:  sdl-keycode->symbol)
  setter: (sdl-keyboard-event-sym-set!
           raw:   sdl-keyboard-event-sym-raw-set!
           conv:  sdl-symbol->keycode))

(define-enum-mask-accessor
  getter: (sdl-keyboard-event-mod
           raw:    sdl-keyboard-event-mod-raw
           unpack: sdl-unpack-keymods
           exact:  #f)
  setter: (sdl-keyboard-event-mod-set!
           raw:    sdl-keyboard-event-mod-raw-set!
           pack:   sdl-pack-keymods))
