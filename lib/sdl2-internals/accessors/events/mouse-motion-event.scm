;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-mouse-motion-event?
        sdl-mouse-motion-event-window-id
        sdl-mouse-motion-event-window-id-set!
        sdl-mouse-motion-event-which
        sdl-mouse-motion-event-which-set!
        sdl-mouse-motion-event-state-raw
        sdl-mouse-motion-event-state-raw-set!
        sdl-mouse-motion-event-state
        sdl-mouse-motion-event-state-set!
        sdl-mouse-motion-event-x
        sdl-mouse-motion-event-x-set!
        sdl-mouse-motion-event-y
        sdl-mouse-motion-event-y-set!
        sdl-mouse-motion-event-xrel
        sdl-mouse-motion-event-xrel-set!
        sdl-mouse-motion-event-yrel
        sdl-mouse-motion-event-yrel-set!)


(define-sdl-event-type "SDL_MouseMotionEvent"
  types: (SDL_MOUSEMOTION)
  pred:  sdl-mouse-motion-event?
  print: ((state sdl-mouse-motion-event-state)
          (x sdl-mouse-motion-event-x)
          (y sdl-mouse-motion-event-y)
          (xrel sdl-mouse-motion-event-xrel)
          (yrel sdl-mouse-motion-event-yrel))
  ("motion.windowID"
   type:   Uint32
   getter: sdl-mouse-motion-event-window-id
   setter: sdl-mouse-motion-event-window-id-set!
   guard:  (Uint32-guard "sdl-mouse-motion-event field windowID"))
  ("motion.which"
   type:   Uint32
   getter: sdl-mouse-motion-event-which
   setter: sdl-mouse-motion-event-which-set!
   guard:  (Uint32-guard "sdl-mouse-motion-event field which"))
  ("motion.state"
   type:   Uint32
   getter: sdl-mouse-motion-event-state-raw
   setter: sdl-mouse-motion-event-state-raw-set!
   guard:  (Uint32-guard "sdl-mouse-motion-event field state"))
  ("motion.x"
   type:   Sint32
   getter: sdl-mouse-motion-event-x
   setter: sdl-mouse-motion-event-x-set!
   guard:  (Sint32-guard "sdl-mouse-motion-event field x"))
  ("motion.y"
   type:   Sint32
   getter: sdl-mouse-motion-event-y
   setter: sdl-mouse-motion-event-y-set!
   guard:  (Sint32-guard "sdl-mouse-motion-event field y"))
  ("motion.xrel"
   type:   Sint32
   getter: sdl-mouse-motion-event-xrel
   setter: sdl-mouse-motion-event-xrel-set!
   guard:  (Sint32-guard "sdl-mouse-motion-event field xrel"))
  ("motion.yrel"
   type:   Sint32
   getter: sdl-mouse-motion-event-yrel
   setter: sdl-mouse-motion-event-yrel-set!
   guard:  (Sint32-guard "sdl-mouse-motion-event field yrel")))


(define-enum-mask-accessor
  getter: (sdl-mouse-motion-event-state
           raw:    sdl-mouse-motion-event-state-raw
           unpack: sdl-unpack-mouse-button-masks
           exact:  #f)
  setter: (sdl-mouse-motion-event-state-set!
           raw:    sdl-mouse-motion-event-state-raw-set!
           pack:   sdl-pack-mouse-button-masks))
