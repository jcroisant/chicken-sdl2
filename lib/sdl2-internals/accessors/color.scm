;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-color-r  sdl-color-r-set!
        sdl-color-g  sdl-color-g-set!
        sdl-color-b  sdl-color-b-set!
        sdl-color-a  sdl-color-a-set!
        sdl-make-color
        sdl-color-set!
        sdl-color->list

        sdl-colour-r  sdl-colour-r-set!
        sdl-colour-g  sdl-colour-g-set!
        sdl-colour-b  sdl-colour-b-set!
        sdl-colour-a  sdl-colour-a-set!
        sdl-make-colour
        sdl-colour-set!
        sdl-colour->list)


(define-struct-field-accessors
  SDL_Color*
  sdl-color?
  ("r"
   type:   Uint8
   getter: sdl-color-r
   setter: sdl-color-r-set!
   guard:  (Uint8-guard "sdl-color field r"))
  ("g"
   type:   Uint8
   getter: sdl-color-g
   setter: sdl-color-g-set!
   guard:  (Uint8-guard "sdl-color field g"))
  ("b"
   type:   Uint8
   getter: sdl-color-b
   setter: sdl-color-b-set!
   guard:  (Uint8-guard "sdl-color field b"))
  ("a"
   type:   Uint8
   getter: sdl-color-a
   setter: sdl-color-a-set!
   guard:  (Uint8-guard "sdl-color field a")))


(define (sdl-make-color #!optional (r 0) (g 0) (b 0) (a 255))
  (let ((color (sdl-alloc-color)))
    (sdl-color-set! color r g b a)
    color))

(define (sdl-color-set! color #!optional r g b a)
  (when r (sdl-color-r-set! color r))
  (when g (sdl-color-g-set! color g))
  (when b (sdl-color-b-set! color b))
  (when a (sdl-color-a-set! color a))
  color)

(define (sdl-color->list color)
  (list (sdl-color-r color)
        (sdl-color-g color)
        (sdl-color-b color)
        (sdl-color-a color)))


(define sdl-colour-r      sdl-color-r)
(define sdl-colour-g      sdl-color-g)
(define sdl-colour-b      sdl-color-b)
(define sdl-colour-a      sdl-color-a)
(define sdl-colour-r-set! sdl-color-r-set!)
(define sdl-colour-g-set! sdl-color-g-set!)
(define sdl-colour-b-set! sdl-color-b-set!)
(define sdl-colour-a-set! sdl-color-a-set!)
(define sdl-make-colour   sdl-make-color)
(define sdl-colour-set!   sdl-color-set!)
(define sdl-colour->list  sdl-color->list)
