;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-rect-x  sdl-rect-x-set!
        sdl-rect-y  sdl-rect-y-set!
        sdl-rect-w  sdl-rect-w-set!
        sdl-rect-h  sdl-rect-h-set!
        sdl-make-rect
        sdl-rect-set!
        sdl-rect->list)


(define-struct-field-accessors
  SDL_Rect*
  sdl-rect?
  ("x"
   type:   Sint32
   getter: sdl-rect-x
   setter: sdl-rect-x-set!
   guard:  (Sint32-guard "sdl-rect field x"))
  ("y"
   type:   Sint32
   getter: sdl-rect-y
   setter: sdl-rect-y-set!
   guard:  (Sint32-guard "sdl-rect field y"))
  ("w"
   type:   Sint32
   getter: sdl-rect-w
   setter: sdl-rect-w-set!
   guard:  (Sint32-guard "sdl-rect field w"))
  ("h"
   type:   Sint32
   getter: sdl-rect-h
   setter: sdl-rect-h-set!
   guard:  (Sint32-guard "sdl-rect field h")))


(define (sdl-make-rect #!optional (x 0) (y 0) (w 0) (h 0))
  (let ((rect (sdl-alloc-rect)))
    (sdl-rect-set! rect x y w h)
    rect))

(define (sdl-rect-set! rect #!optional x y w h)
  (when x (sdl-rect-x-set! rect x))
  (when y (sdl-rect-y-set! rect y))
  (when w (sdl-rect-w-set! rect w))
  (when h (sdl-rect-h-set! rect h))
  rect)

(define (sdl-rect->list rect)
  (list (sdl-rect-x rect)
        (sdl-rect-y rect)
        (sdl-rect-w rect)
        (sdl-rect-h rect)))
