;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-display-mode-format        sdl-display-mode-format-set!
        sdl-display-mode-w             sdl-display-mode-w-set!
        sdl-display-mode-h             sdl-display-mode-h-set!
        sdl-display-mode-refresh-rate  sdl-display-mode-refresh-rate-set!
        %sdl-display-mode-driverdata)

(define-struct-field-accessors
  SDL_DisplayMode*
  sdl-display-mode?
  ("format"
   type:   Uint32
   getter: sdl-display-mode-format
   setter: sdl-display-mode-format-set!
   guard:  (Uint32-guard "sdl-display-mode field format"))
  ("w"
   type:   int
   getter: sdl-display-mode-w
   setter: sdl-display-mode-w-set!
   guard:  (int-guard "sdl-display-mode field w"))
  ("h"
   type:   int
   getter: sdl-display-mode-h
   setter: sdl-display-mode-h-set!
   guard:  (int-guard "sdl-display-mode field h"))
  ("refresh_rate"
   type:   int
   getter: sdl-display-mode-refresh-rate
   setter: sdl-display-mode-refresh-rate-set!
   guard:  (int-guard "sdl-display-mode field refresh-rate"))
  ("driverdata"
   type:   c-pointer
   getter: %sdl-display-mode-driverdata))
