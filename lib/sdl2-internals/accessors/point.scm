;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-point-x  sdl-point-x-set!
        sdl-point-y  sdl-point-y-set!
        sdl-make-point
        sdl-point-set!
        sdl-point->list)


(define-struct-field-accessors
  SDL_Point*
  sdl-point?
  ("x"
   type:   Sint32
   getter: sdl-point-x
   setter: sdl-point-x-set!
   guard:  (Sint32-guard "sdl-point field x"))
  ("y"
   type:   Sint32
   getter: sdl-point-y
   setter: sdl-point-y-set!
   guard:  (Sint32-guard "sdl-point field y")))


(define (sdl-make-point #!optional (x 0) (y 0))
  (let ((point (sdl-alloc-point)))
    (sdl-point-set! point x y)
    point))

(define (sdl-point-set! point #!optional x y)
  (when x (sdl-point-x-set! point x))
  (when y (sdl-point-y-set! point y))
  point)

(define (sdl-point->list point)
  (list (sdl-point-x point)
        (sdl-point-y point)))
