;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-version-major  sdl-version-major-set!
        sdl-version-minor  sdl-version-minor-set!
        sdl-version-patch  sdl-version-patch-set!
        sdl-make-version
        sdl-version-set!
        sdl-version->list)


(define-struct-field-accessors
  SDL_version*
  sdl-version?
  ("major"
   type:   Uint8
   getter: sdl-version-major
   setter: sdl-version-major-set!
   guard:  (Uint8-guard "sdl-version field major"))
  ("minor"
   type:   Uint8
   getter: sdl-version-minor
   setter: sdl-version-minor-set!
   guard:  (Uint8-guard "sdl-version field minor"))
  ("patch"
   type:   Uint8
   getter: sdl-version-patch
   setter: sdl-version-patch-set!
   guard:  (Uint8-guard "sdl-version field patch")))


(define (sdl-make-version #!optional (major 0) (minor 0) (patch 0))
  (let ((version (sdl-alloc-version)))
    (sdl-version-set! version major minor patch)
    version))

(define (sdl-version-set! version #!optional major minor patch)
  (when major (sdl-version-major-set! version major))
  (when minor (sdl-version-minor-set! version minor))
  (when patch (sdl-version-patch-set! version patch))
  version)

(define (sdl-version->list version)
  (list (sdl-version-major version)
        (sdl-version-minor version)
        (sdl-version-patch version)))
