;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-color?
        %sdl-wrap-color
        %sdl-unwrap-color
        %sdl-color-pointer
        %sdl-color-pointer-set!
        sdl-free-color!
        sdl-alloc-color*
        sdl-alloc-color

        sdl-colour?
        sdl-free-colour!
        sdl-alloc-colour*
        sdl-alloc-colour)

(define-struct-record-type
  sdl-color "SDL_Color"
  pred:    sdl-color?
  wrap:    %sdl-wrap-color
  unwrap:  %sdl-unwrap-color
  (pointer %sdl-color-pointer
           %sdl-color-pointer-set!))

(define-struct-memory-helpers
  "SDL_Color"
  using: (%sdl-wrap-color
          sdl-color?
          %sdl-color-pointer
          %sdl-color-pointer-set!)
  define: (sdl-free-color!
           sdl-alloc-color*
           sdl-alloc-color))

(define-struct-record-printer sdl-color
  %sdl-color-pointer
  show-address: #f
  (#f sdl-color->list))

(define sdl-colour?       sdl-color?)
(define sdl-free-colour!  sdl-free-color!)
(define sdl-alloc-colour* sdl-alloc-color*)
(define sdl-alloc-colour  sdl-alloc-color)
