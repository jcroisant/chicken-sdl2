;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-pixel-format?
        %sdl-wrap-pixel-format
        %sdl-unwrap-pixel-format
        %sdl-pixel-format-pointer
        %sdl-pixel-format-pointer-set!

        sdl-free-pixel-format!
        sdl-alloc-pixel-format
        sdl-alloc-pixel-format*)


(define-struct-record-type
  sdl-pixel-format "SDL_PixelFormat"
  pred:    sdl-pixel-format?
  wrap:    %sdl-wrap-pixel-format
  unwrap:  %sdl-unwrap-pixel-format
  (pointer %sdl-pixel-format-pointer
           %sdl-pixel-format-pointer-set!))


(define (sdl-free-pixel-format! pixel-format)
  (define foreign-freer
    ;; Cannot use SDL_PixelFormat* foreign type because it has
    ;; not been defined yet.
    (foreign-lambda void "SDL_FreeFormat"
                    (c-pointer "SDL_PixelFormat")))
  (assert (sdl-pixel-format? pixel-format))
  (unless (sdl-struct-null? pixel-format)
    (foreign-freer (%sdl-pixel-format-pointer pixel-format))
    (%sdl-nullify-struct! pixel-format))
  pixel-format)


(define (sdl-alloc-pixel-format pixel-format-enum)
  (set-finalizer! (sdl-alloc-pixel-format* pixel-format-enum)
                  sdl-free-pixel-format!))

(define (sdl-alloc-pixel-format* pixel-format-enum)
  (define foreign-alloc
    ;; Cannot use SDL_PixelFormat* foreign type because it has not
    ;; been defined yet.
    (foreign-lambda (c-pointer "SDL_PixelFormat")
                    "SDL_AllocFormat" Uint32))
  (define (err x)
    (error 'sdl-alloc-pixel-format* "invalid pixel format enum" x))
  (%sdl-wrap-pixel-format
   (foreign-alloc
    (cond ((integer? pixel-format-enum)
           pixel-format-enum)
          (else
           (sdl-symbol->pixel-format-enum
            pixel-format-enum err))))))


(define-struct-record-printer sdl-pixel-format
  %sdl-pixel-format-pointer
  show-address: #f
  (#f sdl-pixel-format-format)
  (palette sdl-pixel-format-palette))
