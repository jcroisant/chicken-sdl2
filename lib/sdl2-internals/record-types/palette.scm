;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-palette?
        %sdl-wrap-palette
        %sdl-unwrap-palette
        %sdl-palette-pointer
        %sdl-palette-pointer-set!

        sdl-free-palette!
        sdl-alloc-palette
        sdl-alloc-palette*)


(define-struct-record-type
  sdl-palette "SDL_Palette"
  pred:    sdl-palette?
  wrap:    %sdl-wrap-palette
  unwrap:  %sdl-unwrap-palette
  (pointer %sdl-palette-pointer
           %sdl-palette-pointer-set!))


(define (sdl-free-palette! palette)
  (define foreign-freer
    ;; Cannot use SDL_Palette* foreign type because it has not been
    ;; defined yet.
    (foreign-lambda void "SDL_FreePalette"
                    (c-pointer "SDL_Palette")))
  (assert (sdl-palette? palette))
  (unless (sdl-struct-null? palette)
    (foreign-freer (%sdl-palette-pointer palette))
    (%sdl-nullify-struct! palette))
  palette)


(define (sdl-alloc-palette ncolors)
  (set-finalizer! (sdl-alloc-palette* ncolors)
                  sdl-free-palette!))

(define (sdl-alloc-palette* ncolors)
  (define foreign-alloc
    ;; Cannot use SDL_Palette* foreign type because it has not been
    ;; defined yet.
    (foreign-lambda (c-pointer "SDL_Palette")
                    "SDL_AllocPalette" int))
  (%sdl-wrap-palette (foreign-alloc ncolors)))


(define-struct-record-printer sdl-palette
  %sdl-palette-pointer
  show-address: #t
  (ncolors sdl-palette-ncolors))

