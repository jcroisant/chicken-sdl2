;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;; This file re-exports all the sdl2-internals exports that are
;;; intended for users to use directly. They are re-exported so that
;;; users do not need to import the sdl2-internals module.


(reexport
 (only
  sdl2-internals

  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; ACCESSORS

  ;; sdl2-internals/accessors/audio-cvt.scm
  sdl-audio-cvt-needed
  sdl-audio-cvt-src-format
  sdl-audio-cvt-dst-format
  sdl-audio-cvt-rate-incr
  sdl-audio-cvt-buf
  sdl-audio-cvt-len
  sdl-audio-cvt-len-cvt
  sdl-audio-cvt-len-mult
  sdl-audio-cvt-len-ratio

  ;; sdl2-internals/accessors/audio-spec.scm
  sdl-audio-spec-freq      sdl-audio-spec-freq-set!
  sdl-audio-spec-format    sdl-audio-spec-format-set!
  sdl-audio-spec-channels  sdl-audio-spec-channels-set!
  sdl-audio-spec-silence
  sdl-audio-spec-samples   sdl-audio-spec-samples-set!
  sdl-audio-spec-size
  sdl-audio-spec-callback  sdl-audio-spec-callback-set!
  sdl-audio-spec-userdata  sdl-audio-spec-userdata-set!

  ;; sdl2-internals/accessors/color.scm
  sdl-color-r  sdl-color-r-set!
  sdl-color-g  sdl-color-g-set!
  sdl-color-b  sdl-color-b-set!
  sdl-color-a  sdl-color-a-set!
  sdl-make-color
  sdl-color-set!
  sdl-color->list
  sdl-colour-r  sdl-colour-r-set!
  sdl-colour-g  sdl-colour-g-set!
  sdl-colour-b  sdl-colour-b-set!
  sdl-colour-a  sdl-colour-a-set!
  sdl-make-colour
  sdl-colour-set!
  sdl-colour->list

  ;; sdl2-internals/accessors/display-mode.scm
  sdl-display-mode-format        sdl-display-mode-format-set!
  sdl-display-mode-w             sdl-display-mode-w-set!
  sdl-display-mode-h             sdl-display-mode-h-set!
  sdl-display-mode-refresh-rate  sdl-display-mode-refresh-rate-set!


  ;; sdl2-internals/accessors/events/common.scm
  sdl-event-type-raw   sdl-event-type-raw-set!
  sdl-event-type       sdl-event-type-set!
  sdl-event-timestamp  sdl-event-timestamp-set!

  ;; sdl2-internals/accessors/events/controller-axis-event.scm
  sdl-controller-axis-event?
  sdl-controller-axis-event-which  sdl-controller-axis-event-which-set!
  sdl-controller-axis-event-axis   sdl-controller-axis-event-axis-set!
  sdl-controller-axis-event-value  sdl-controller-axis-event-value-set!

  ;; sdl2-internals/accessors/events/controller-button-event.scm
  sdl-controller-button-event?
  sdl-controller-button-event-which   sdl-controller-button-event-which-set!
  sdl-controller-button-event-button  sdl-controller-button-event-button-set!
  sdl-controller-button-event-state   sdl-controller-button-event-state-set!

  ;; sdl2-internals/accessors/events/controller-device-event.scm
  sdl-controller-device-event?
  sdl-controller-device-event-which  sdl-controller-device-event-which-set!

  ;; sdl2-internals/accessors/events/dollar-gesture-event.scm
  sdl-dollar-gesture-event?
  sdl-dollar-gesture-event-touch-id     sdl-dollar-gesture-event-touch-id-set!
  sdl-dollar-gesture-event-gesture-id   sdl-dollar-gesture-event-gesture-id-set!
  sdl-dollar-gesture-event-num-fingers  sdl-dollar-gesture-event-num-fingers-set!
  sdl-dollar-gesture-event-error        sdl-dollar-gesture-event-error-set!
  sdl-dollar-gesture-event-x            sdl-dollar-gesture-event-x-set!
  sdl-dollar-gesture-event-y            sdl-dollar-gesture-event-y-set!

  ;; sdl2-internals/accessors/events/drop-event.scm
  sdl-drop-event?
  sdl-drop-event-file  sdl-drop-event-file-set!

  ;; sdl2-internals/accessors/events/joy-axis-event.scm
  sdl-joy-axis-event?
  sdl-joy-axis-event-which  sdl-joy-axis-event-which-set!
  sdl-joy-axis-event-axis   sdl-joy-axis-event-axis-set!
  sdl-joy-axis-event-value  sdl-joy-axis-event-value-set!

  ;; sdl2-internals/accessors/events/joy-ball-event.scm
  sdl-joy-ball-event?
  sdl-joy-ball-event-which  sdl-joy-ball-event-which-set!
  sdl-joy-ball-event-ball   sdl-joy-ball-event-ball-set!
  sdl-joy-ball-event-xrel   sdl-joy-ball-event-xrel-set!
  sdl-joy-ball-event-yrel   sdl-joy-ball-event-yrel-set!

  ;; sdl2-internals/accessors/events/joy-button-event.scm
  sdl-joy-button-event?
  sdl-joy-button-event-which   sdl-joy-button-event-which-set!
  sdl-joy-button-event-button  sdl-joy-button-event-button-set!
  sdl-joy-button-event-state   sdl-joy-button-event-state-set!

  ;; sdl2-internals/accessors/events/joy-device-event.scm
  sdl-joy-device-event?
  sdl-joy-device-event-which  sdl-joy-device-event-which-set!

  ;; sdl2-internals/accessors/events/joy-hat-event.scm
  sdl-joy-hat-event?
  sdl-joy-hat-event-which      sdl-joy-hat-event-which-set!
  sdl-joy-hat-event-hat        sdl-joy-hat-event-hat-set!
  sdl-joy-hat-event-value-raw  sdl-joy-hat-event-value-raw-set!
  sdl-joy-hat-event-value      sdl-joy-hat-event-value-set!

  ;; sdl2-internals/accessors/events/keyboard-event.scm
  sdl-keyboard-event?
  sdl-keyboard-event-window-id     sdl-keyboard-event-window-id-set!
  sdl-keyboard-event-state         sdl-keyboard-event-state-set!
  sdl-keyboard-event-repeat        sdl-keyboard-event-repeat-set!
  sdl-keyboard-event-keysym        sdl-keyboard-event-keysym-set!
  sdl-keyboard-event-sym-raw       sdl-keyboard-event-sym-raw-set!
  sdl-keyboard-event-sym           sdl-keyboard-event-sym-set!
  sdl-keyboard-event-scancode-raw  sdl-keyboard-event-scancode-raw-set!
  sdl-keyboard-event-scancode      sdl-keyboard-event-scancode-set!
  sdl-keyboard-event-mod-raw       sdl-keyboard-event-mod-raw-set!
  sdl-keyboard-event-mod           sdl-keyboard-event-mod-set!

  ;; sdl2-internals/accessors/events/mouse-button-event.scm
  sdl-mouse-button-event?
  sdl-mouse-button-event-window-id   sdl-mouse-button-event-window-id-set!
  sdl-mouse-button-event-which       sdl-mouse-button-event-which-set!
  sdl-mouse-button-event-button-raw  sdl-mouse-button-event-button-raw-set!
  sdl-mouse-button-event-button      sdl-mouse-button-event-button-set!
  sdl-mouse-button-event-state       sdl-mouse-button-event-state-set!
  sdl-mouse-button-event-x           sdl-mouse-button-event-x-set!
  sdl-mouse-button-event-y           sdl-mouse-button-event-y-set!

  ;; sdl2-internals/accessors/events/mouse-motion-event.scm
  sdl-mouse-motion-event?
  sdl-mouse-motion-event-window-id  sdl-mouse-motion-event-window-id-set!
  sdl-mouse-motion-event-which      sdl-mouse-motion-event-which-set!
  sdl-mouse-motion-event-state-raw  sdl-mouse-motion-event-state-raw-set!
  sdl-mouse-motion-event-state      sdl-mouse-motion-event-state-set!
  sdl-mouse-motion-event-x          sdl-mouse-motion-event-x-set!
  sdl-mouse-motion-event-y          sdl-mouse-motion-event-y-set!
  sdl-mouse-motion-event-xrel       sdl-mouse-motion-event-xrel-set!
  sdl-mouse-motion-event-yrel       sdl-mouse-motion-event-yrel-set!

  ;; sdl2-internals/accessors/events/mouse-wheel-event.scm
  sdl-mouse-wheel-event?
  sdl-mouse-wheel-event-window-id  sdl-mouse-wheel-event-window-id-set!
  sdl-mouse-wheel-event-which      sdl-mouse-wheel-event-which-set!
  sdl-mouse-wheel-event-x          sdl-mouse-wheel-event-x-set!
  sdl-mouse-wheel-event-y          sdl-mouse-wheel-event-y-set!

  ;; sdl2-internals/accessors/events/multi-gesture-event.scm
  sdl-multi-gesture-event?
  sdl-multi-gesture-event-touch-id     sdl-multi-gesture-event-touch-id-set!
  sdl-multi-gesture-event-dtheta       sdl-multi-gesture-event-dtheta-set!
  sdl-multi-gesture-event-ddist        sdl-multi-gesture-event-ddist-set!
  sdl-multi-gesture-event-x            sdl-multi-gesture-event-x-set!
  sdl-multi-gesture-event-y            sdl-multi-gesture-event-y-set!
  sdl-multi-gesture-event-num-fingers  sdl-multi-gesture-event-num-fingers-set!

  ;; sdl2-internals/accessors/events/quit-event.scm
  sdl-quit-event?

  ;; sdl2-internals/accessors/events/sys-wm-event.scm
  sdl-sys-wm-event?
  sdl-sys-wm-event-msg  sdl-sys-wm-event-msg-set!

  ;; sdl2-internals/accessors/events/text-editing-event.scm
  sdl-text-editing-event?
  sdl-text-editing-event-window-id  sdl-text-editing-event-window-id-set!
  sdl-text-editing-event-text       sdl-text-editing-event-text-set!
  sdl-text-editing-event-start      sdl-text-editing-event-start-set!
  sdl-text-editing-event-length     sdl-text-editing-event-length-set!

  ;; sdl2-internals/accessors/events/text-input-event.scm
  sdl-text-input-event?
  sdl-text-input-event-window-id  sdl-text-input-event-window-id-set!
  sdl-text-input-event-text       sdl-text-input-event-text-set!

  ;; sdl2-internals/accessors/events/touch-finger-event.scm
  sdl-touch-finger-event?
  sdl-touch-finger-event-touch-id   sdl-touch-finger-event-touch-id-set!
  sdl-touch-finger-event-finger-id  sdl-touch-finger-event-finger-id-set!
  sdl-touch-finger-event-x          sdl-touch-finger-event-x-set!
  sdl-touch-finger-event-y          sdl-touch-finger-event-y-set!
  sdl-touch-finger-event-dx         sdl-touch-finger-event-dx-set!
  sdl-touch-finger-event-dy         sdl-touch-finger-event-dy-set!
  sdl-touch-finger-event-pressure   sdl-touch-finger-event-pressure-set!

  ;; sdl2-internals/accessors/events/user-event.scm
  sdl-user-event?
  sdl-user-event-window-id  sdl-user-event-window-id-set!
  sdl-user-event-code       sdl-user-event-code-set!
  sdl-user-event-data1      sdl-user-event-data1-set!
  sdl-user-event-data2      sdl-user-event-data2-set!

  ;; sdl2-internals/accessors/events/window-event.scm
  sdl-window-event?
  sdl-window-event-window-id  sdl-window-event-window-id-set!
  sdl-window-event-event-raw  sdl-window-event-event-raw-set!
  sdl-window-event-event      sdl-window-event-event-set!
  sdl-window-event-data1      sdl-window-event-data1-set!
  sdl-window-event-data2      sdl-window-event-data2-set!


  ;; sdl2-internals/accessors/finger.scm
  sdl-finger-id
  sdl-finger-x
  sdl-finger-y
  sdl-finger-pressure

  ;; sdl2-internals/accessors/keysym.scm
  sdl-keysym-scancode-raw  sdl-keysym-scancode-raw-set!
  sdl-keysym-sym-raw       sdl-keysym-sym-raw-set!
  sdl-keysym-mod-raw       sdl-keysym-mod-raw-set!
  sdl-keysym-scancode      sdl-keysym-scancode-set!
  sdl-keysym-sym           sdl-keysym-sym-set!
  sdl-keysym-mod           sdl-keysym-mod-set!

  ;; sdl2-internals/accessors/palette.scm
  sdl-palette-ncolors

  ;; sdl2-internals/accessors/pixel-format.scm
  sdl-pixel-format-format-raw
  sdl-pixel-format-format
  sdl-pixel-format-palette  sdl-pixel-format-palette-set!
  sdl-pixel-format-bits-per-pixel
  sdl-pixel-format-bytes-per-pixel
  sdl-pixel-format-rmask
  sdl-pixel-format-gmask
  sdl-pixel-format-bmask
  sdl-pixel-format-amask

  ;; sdl2-internals/accessors/point.scm
  sdl-point-x  sdl-point-x-set!
  sdl-point-y  sdl-point-y-set!
  sdl-make-point
  sdl-point-set!
  sdl-point->list

  ;; sdl2-internals/accessors/rect.scm
  sdl-rect-x  sdl-rect-x-set!
  sdl-rect-y  sdl-rect-y-set!
  sdl-rect-w  sdl-rect-w-set!
  sdl-rect-h  sdl-rect-h-set!
  sdl-make-rect
  sdl-rect-set!
  sdl-rect->list

  ;; sdl2-internals/accessors/rwops.scm
  sdl-rwops-type
  sdl-rwops-type-raw

  ;; sdl2-internals/accessors/surface.scm
  sdl-surface-format
  sdl-surface-w
  sdl-surface-h
  sdl-surface-pitch
  sdl-surface-refcount   sdl-surface-refcount-set!

  ;; sdl2-internals/accessors/version.scm
  sdl-version-major  sdl-version-major-set!
  sdl-version-minor  sdl-version-minor-set!
  sdl-version-patch  sdl-version-patch-set!
  sdl-make-version
  sdl-version-set!
  sdl-version->list


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; RECORD TYPES

  ;; sdl2-internals/record-types/audio-cvt.scm
  sdl-audio-cvt?

  ;; sdl2-internals/record-types/audio-spec.scm
  sdl-audio-spec?

  ;; sdl2-internals/record-types/color.scm
  sdl-color?
  sdl-free-color!
  sdl-alloc-color*
  sdl-alloc-color
  sdl-colour?
  sdl-free-colour!
  sdl-alloc-colour*
  sdl-alloc-colour

  ;; sdl2-internals/record-types/cursor.scm
  sdl-cursor?

  ;; sdl2-internals/record-types/display-mode.scm
  sdl-display-mode?
  sdl-free-display-mode!
  sdl-alloc-display-mode*
  sdl-alloc-display-mode

  ;; sdl2-internals/record-types/event.scm
  sdl-event?
  sdl-free-event!
  sdl-alloc-event*
  sdl-alloc-event

  ;; sdl2-internals/record-types/finger.scm
  sdl-finger?

  ;; sdl2-internals/record-types/gl-context.scm
  sdl-gl-context?

  ;; sdl2-internals/record-types/joystick-guid.scm
  sdl-joystick-guid?

  ;; sdl2-internals/record-types/joystick.scm
  sdl-joystick-guid?

  ;; sdl2-internals/record-types/keysym.scm
  sdl-keysym?
  sdl-free-keysym!
  sdl-alloc-keysym*
  sdl-alloc-keysym

  ;; sdl2-internals/record-types/palette.scm
  sdl-palette?
  sdl-free-palette!
  sdl-alloc-palette
  sdl-alloc-palette*

  ;; sdl2-internals/record-types/pixel-format.scm
  sdl-pixel-format?
  sdl-free-pixel-format!
  sdl-alloc-pixel-format
  sdl-alloc-pixel-format*

  ;; sdl2-internals/record-types/point.scm
  sdl-point?
  sdl-free-point!
  sdl-alloc-point*
  sdl-alloc-point

  ;; sdl2-internals/record-types/rect.scm
  sdl-rect?
  sdl-free-rect!
  sdl-alloc-rect*
  sdl-alloc-rect

  ;; sdl2-internals/record-types/rwops.scm
  sdl-rwops?

  ;; sdl2-internals/record-types/surface.scm
  sdl-surface?

  ;; sdl2-internals/record-types/sys-wm-info.scm
  sdl-sys-wm-info?

  ;; sdl2-internals/record-types/sys-wm-msg.scm
  sdl-sys-wm-msg?

  ;; sdl2-internals/record-types/texture.scm
  sdl-texture?

  ;; sdl2-internals/record-types/version.scm
  sdl-version?
  sdl-free-version!
  sdl-alloc-version*
  sdl-alloc-version

  ;; sdl2-internals/record-types/window.scm
  sdl-window?


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; MISC

  ;; sdl2-internals/helpers/struct.scm
  sdl-struct-null?

  ))
