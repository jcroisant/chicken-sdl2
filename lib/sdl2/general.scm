;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-init!
        sdl-init-subsystem!
        sdl-quit!
        sdl-quit-subsystem!
        sdl-was-init
        sdl-set-main-ready!

        sdl-clear-error!
        sdl-get-error
        sdl-set-error!

        sdl-get-platform

        sdl-get-version
        sdl-get-compiled-version
        sdl-version-at-least?)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION AND SHUTDOWN

(define (sdl-init! flags)
  (= 0 (SDL_Init (sdl-pack-init-flags flags))))

(define (sdl-init-subsystem! flags)
  (= 0 (SDL_InitSubSystem (sdl-pack-init-flags flags))))

(define (sdl-quit!)
  (SDL_Quit))

(define (sdl-quit-subsystem! flags)
  (SDL_QuitSubSystem (sdl-pack-init-flags flags)))

(define (sdl-was-init flags)
  (sdl-unpack-init-flags
   (SDL_WasInit (sdl-pack-init-flags flags))
   #t))

(define (sdl-set-main-ready!)
  (SDL_SetMainReady))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ERROR HANDLING

(define (sdl-clear-error!)
  (SDL_ClearError))

(define (sdl-get-error)
  (SDL_GetError))

(define (sdl-set-error! message)
  (SDL_SetError message))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PLATFORM

(define (sdl-get-platform)
  (SDL_GetPlatform))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VERSION

(define (sdl-get-version)
  (let ((v (sdl-make-version)))
    (SDL_GetVersion v)
    v))

(define (sdl-get-compiled-version)
  (let ((v (sdl-make-version)))
    (SDL_VERSION v)
    v))

(define (sdl-version-at-least? x y z)
  (SDL_VERSION_ATLEAST x y z))
