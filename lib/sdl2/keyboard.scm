;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-get-key-from-name       sdl-get-key-from-name-raw
        sdl-get-key-from-scancode   sdl-get-key-from-scancode-raw
        sdl-get-key-name

        sdl-get-scancode-from-name  sdl-get-scancode-from-name-raw
        sdl-get-scancode-from-key   sdl-get-scancode-from-key-raw
        sdl-get-scancode-name

        sdl-get-keyboard-focus
        sdl-keyboard-scancode-pressed?

        sdl-mod-state  sdl-mod-state-raw
        sdl-mod-state-set!

        sdl-text-input-rect-set!
        sdl-start-text-input!
        sdl-stop-text-input!
        sdl-text-input-active?

        sdl-screen-keyboard-support?
        sdl-screen-keyboard-shown?)



(define (sdl-get-key-from-name name-str)
  (sdl-keycode->symbol
   (sdl-get-key-from-name-raw name-str)))

(define (sdl-get-key-from-name-raw name-str)
  (SDL_GetKeyFromName name-str))

(define (sdl-get-key-from-scancode scancode)
  (sdl-keycode->symbol
   (sdl-get-key-from-scancode-raw scancode)))

(define (sdl-get-key-from-scancode-raw scancode)
  (SDL_GetKeyFromScancode (if (integer? scancode)
                              scancode
                              (sdl-symbol->scancode scancode))))

(define (sdl-get-key-name key)
  (SDL_GetKeyName (if (integer? key)
                      key
                      (sdl-symbol->keycode key))))


(define (sdl-get-scancode-from-name name-str)
  (sdl-scancode->symbol
   (sdl-get-scancode-from-name-raw name-str)))

(define (sdl-get-scancode-from-name-raw name-str)
  (SDL_GetScancodeFromName name-str))

(define (sdl-get-scancode-from-key key)
  (sdl-scancode->symbol
   (sdl-get-scancode-from-key-raw key)))

(define (sdl-get-scancode-from-key-raw key)
  (SDL_GetScancodeFromKey (if (integer? key)
                              key
                              (sdl-symbol->keycode key))))

(define (sdl-get-scancode-name scancode)
  (SDL_GetScancodeName (if (integer? scancode)
                           scancode
                           (sdl-symbol->scancode scancode))))


(define (sdl-get-keyboard-focus)
  (SDL_GetKeyboardFocus))


;;; Related to SDL_GetKeyboardState.
(define (sdl-keyboard-scancode-pressed? scancode)
  (case (%sdl-query-keyboard-state
         (if (integer? scancode)
             scancode
             (sdl-symbol->scancode scancode)))
    ((1) #t)
    ((0) #f)
    (else
     (error 'sdl-keyboard-scancode-pressed?
            "invalid scancode" scancode))))


(define (sdl-mod-state-set! mods)
  (SDL_SetModState (sdl-pack-keymods mods)))

(define (sdl-mod-state)
  (sdl-unpack-keymods
   (sdl-mod-state-raw)))

(set! (setter sdl-mod-state)
      sdl-mod-state-set!)

(define (sdl-mod-state-raw)
  (SDL_GetModState))

(set! (setter sdl-mod-state-raw)
      sdl-mod-state-set!)


(define (sdl-text-input-rect-set! rect-or-false)
  (SDL_SetTextInputRect rect-or-false))

(define (sdl-start-text-input!)
  (SDL_StartTextInput))

(define (sdl-stop-text-input!)
  (SDL_StopTextInput))

(define (sdl-text-input-active?)
  (SDL_IsTextInputActive))


(define (sdl-screen-keyboard-support?)
  (SDL_HasScreenKeyboardSupport))

(define (sdl-screen-keyboard-shown? window)
  (SDL_IsScreenKeyboardShown window))
