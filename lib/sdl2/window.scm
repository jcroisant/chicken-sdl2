;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-create-window!
        sdl-get-window-from-id
        sdl-destroy-window!

        sdl-update-window-surface!
        sdl-update-window-surface-rects!

        sdl-show-window!
        sdl-hide-window!
        sdl-maximize-window!
        sdl-minimize-window!
        sdl-raise-window!
        sdl-restore-window!

        sdl-window-bordered-set!
        sdl-window-brightness      sdl-window-brightness-set!
        ;; TODO: sdl-window-data
        ;; TODO: sdl-window-data-set!
        sdl-window-display-index
        sdl-window-display-mode    sdl-window-display-mode-set!
        sdl-window-flags
        sdl-window-fullscreen-set!
        ;; TODO: sdl-window-gamma-ramp
        ;; TODO: sdl-window-gamma-ramp-set!
        sdl-window-grab            sdl-window-grab-set!
        sdl-window-icon-set!
        sdl-window-id
        sdl-window-maximum-size    sdl-window-maximum-size-set!
        sdl-window-minimum-size    sdl-window-minimum-size-set!
        sdl-window-pixel-format
        sdl-window-position        sdl-window-position-set!
        sdl-window-size            sdl-window-size-set!
        sdl-window-surface
        sdl-window-title           sdl-window-title-set!
        ;; TODO: sdl-window-wm-info
        )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CREATE / GET / DESTROY WINDOW

(define (%window-pos->int pos x-or-y)
  (case pos
    ((undefined) SDL_WINDOWPOS_UNDEFINED)
    ((centered)  SDL_WINDOWPOS_CENTERED)
    (else
     (if (integer? pos)
         pos
         (error 'sdl-create-window!
                (sprintf "invalid window ~A position" x-or-y)
                pos)))))

(define (sdl-create-window! title x y w h #!optional (flags '()))
  (SDL_CreateWindow
   title
   (%window-pos->int x "x") (%window-pos->int y "y")
   w h
   (sdl-pack-window-flags flags)))


(define (sdl-get-window-from-id id)
  (SDL_GetWindowFromID id))

(define (sdl-destroy-window! window)
  (SDL_DestroyWindow window))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UPDATE WINDOW SURFACE

(define (sdl-update-window-surface! window)
  (= 0 (SDL_UpdateWindowSurface window)))

(define (sdl-update-window-surface-rects! window rects)
  (assert (every sdl-rect? rects))
  (with-temp-mem ((rect-array (%sdl-rect-list->array rects)))
    (= 0 (SDL_UpdateWindowSurfaceRects
          window rect-array (length rects)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW MANAGEMENT

(define (sdl-show-window! window)
  (SDL_ShowWindow window))

(define (sdl-hide-window! window)
  (SDL_HideWindow window))

(define (sdl-maximize-window! window)
  (SDL_MaximizeWindow window))

(define (sdl-minimize-window! window)
  (SDL_MinimizeWindow window))

(define (sdl-raise-window! window)
  (SDL_RaiseWindow window))

(define (sdl-restore-window! window)
  (SDL_RestoreWindow window))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW PROPERTIES


(define (sdl-window-bordered-set! window bordered?)
  (SDL_SetWindowBordered window bordered?))


(define (sdl-window-brightness-set! window brightness)
  (SDL_SetWindowBrightness window brightness))

(define (sdl-window-brightness window)
  (SDL_GetWindowBrightness window))

(set! (setter sdl-window-brightness)
      sdl-window-brightness-set!)


;; TODO: sdl-window-data
;; TODO: sdl-window-data-set!


(define (sdl-window-display-index window)
  (SDL_GetWindowDisplayIndex window))


(define (sdl-window-display-mode-set! window mode)
  (SDL_SetWindowDisplayMode window mode))

(define (sdl-window-display-mode window)
  (let ((display-mode (sdl-alloc-display-mode)))
    (SDL_GetWindowDisplayMode window display-mode)
    display-mode))

(set! (setter sdl-window-display-mode)
      sdl-window-display-mode-set!)


(define (sdl-window-flags window)
  (sdl-unpack-window-flags
   (SDL_GetWindowFlags window)
   #t))


(define (sdl-window-fullscreen-set! window mode)
  (SDL_SetWindowFullscreen
   window
   (case mode
     ((#t fullscreen) SDL_WINDOW_FULLSCREEN)
     ((fullscreen-desktop) SDL_WINDOW_FULLSCREEN_DESKTOP)
     ((#f) 0)
     (else mode))))


;; TODO: sdl-window-gamma-ramp
;; TODO: sdl-window-gamma-ramp-set!


(define (sdl-window-grab-set! window grab?)
  (SDL_SetWindowGrab window grab?))

(define (sdl-window-grab window)
  (SDL_GetWindowGrab window))

(set! (setter sdl-window-grab)
      sdl-window-grab-set!)


(define (sdl-window-icon-set! window icon)
  (SDL_SetWindowIcon window icon))


(define (sdl-window-id window)
  (SDL_GetWindowID window))


(define (sdl-window-maximum-size-set! window size)
  (SDL_SetWindowMaximumSize window (car size) (cadr size)))

(define (sdl-window-maximum-size window)
  (with-temp-mem ((w-out (%sdl-allocate-int))
                  (h-out (%sdl-allocate-int)))
    (SDL_GetWindowMaximumSize window w-out h-out)
    (values (pointer-s32-ref w-out)
            (pointer-s32-ref h-out))))

(set! (setter sdl-window-maximum-size)
      sdl-window-maximum-size-set!)


(define (sdl-window-minimum-size-set! window size)
  (SDL_SetWindowMinimumSize window (car size) (cadr size)))

(define (sdl-window-minimum-size window)
  (with-temp-mem ((w-out (%sdl-allocate-int))
                  (h-out (%sdl-allocate-int)))
    (SDL_GetWindowMinimumSize window w-out h-out)
    (values (pointer-s32-ref w-out)
            (pointer-s32-ref h-out))))

(set! (setter sdl-window-minimum-size)
      sdl-window-minimum-size-set!)


(define (sdl-window-pixel-format window)
  (SDL_GetWindowPixelFormat window))


(define (sdl-window-position-set! window pos)
  (SDL_SetWindowPosition
   window
   (%window-pos->int (car pos) "x")
   (%window-pos->int (cadr pos) "y")))

(define (sdl-window-position window)
  (with-temp-mem ((x-out (%sdl-allocate-int))
                  (y-out (%sdl-allocate-int)))
    (SDL_GetWindowPosition window x-out y-out)
    (values (pointer-s32-ref x-out)
            (pointer-s32-ref y-out))))

(set! (setter sdl-window-position)
      sdl-window-position-set!)


(define (sdl-window-size-set! window size)
  (SDL_SetWindowSize window (car size) (cadr size)))

(define (sdl-window-size window)
  (with-temp-mem ((w-out (%sdl-allocate-int))
                  (h-out (%sdl-allocate-int)))
    (SDL_GetWindowSize window w-out h-out)
    (values (pointer-s32-ref w-out)
            (pointer-s32-ref h-out))))

(set! (setter sdl-window-size)
      sdl-window-size-set!)


(define (sdl-window-surface window)
  (SDL_GetWindowSurface window))


(define (sdl-window-title-set! window title)
  (SDL_SetWindowTitle window title))

(define (sdl-window-title window)
  (SDL_GetWindowTitle window))

(set! (setter sdl-window-title)
      sdl-window-title-set!)


;; TODO: sdl-window-wm-info
