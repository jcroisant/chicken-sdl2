;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-num-joysticks
        sdl-joystick-open!
        sdl-joystick-close!
        sdl-joystick-update!
        sdl-joystick-event-state  sdl-joystick-event-state-set!
        sdl-joystick-attached?

        sdl-joystick-num-axes
        sdl-joystick-num-balls
        sdl-joystick-num-buttons
        sdl-joystick-num-hats

        sdl-joystick-get-axis
        sdl-joystick-get-ball
        sdl-joystick-get-button
        sdl-joystick-get-hat
        sdl-joystick-get-hat-raw

        sdl-joystick-instance-id
        sdl-joystick-name
        sdl-joystick-name-for-index

        ;; TODO: sdl-joystick-get-device-guid
        ;; TODO: sdl-joystick-get-guid
        ;; TODO: sdl-joystick-get-guid-from-string
        ;; TODO: sdl-joystick-get-guid-string
        )



(define (sdl-num-joysticks)
  (SDL_NumJoysticks))

(define (sdl-joystick-open! device-index)
  (SDL_JoystickOpen device-index))

(define (sdl-joystick-close! joystick)
  (SDL_JoystickClose joystick))

(define (sdl-joystick-update!)
  (SDL_JoystickUpdate))


(define (sdl-joystick-event-state-set! state)
  (let ((result (SDL_JoystickEventState
                 (if state SDL_ENABLE SDL_IGNORE))))
    (if (< result 0)
        result
        (= result SDL_ENABLE))))

(define (sdl-joystick-event-state)
  (= SDL_ENABLE
     (SDL_JoystickEventState SDL_QUERY)))

(set! (setter sdl-joystick-event-state)
      sdl-joystick-event-state-set!)


(define (sdl-joystick-attached? joystick)
  (SDL_JoystickGetAttached joystick))


(define (sdl-joystick-num-axes joystick)
  (SDL_JoystickNumAxes joystick))

(define (sdl-joystick-num-balls joystick)
  (SDL_JoystickNumBalls joystick))

(define (sdl-joystick-num-buttons joystick)
  (SDL_JoystickNumButtons joystick))

(define (sdl-joystick-num-hats joystick)
  (SDL_JoystickNumHats joystick))


(define (sdl-joystick-get-axis joystick axis)
  (SDL_JoystickGetAxis joystick axis))

(define (sdl-joystick-get-ball joystick ball)
  (with-temp-mem ((dx-out (%sdl-allocate-int))
                  (dy-out (%sdl-allocate-int)))
    (let ((response (SDL_JoystickGetBall
                     joystick ball dx-out dy-out)))
      (if (zero? response)
          (values (pointer-s32-ref dx-out)
                  (pointer-s32-ref dy-out))
          (values response (void))))))

(define (sdl-joystick-get-button joystick button)
  (SDL_JoystickGetButton joystick button))

(define (sdl-joystick-get-hat joystick hat)
  (sdl-joystick-hat-position->symbol
   (sdl-joystick-get-hat-raw joystick hat)))

(define (sdl-joystick-get-hat-raw joystick hat)
  (SDL_JoystickGetHat joystick hat))


(define (sdl-joystick-instance-id joystick)
  (SDL_JoystickInstanceID joystick))

(define (sdl-joystick-name joystick)
  (SDL_JoystickName joystick))

(define (sdl-joystick-name-for-index device-index)
  (SDL_JoystickNameForIndex device-index))


;; TODO: sdl-joystick-get-device-guid
;; TODO: sdl-joystick-get-guid
;; TODO: sdl-joystick-get-guid-from-string
;; TODO: sdl-joystick-get-guid-string
