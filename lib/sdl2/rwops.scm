;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-rw-from-file
        sdl-rw-from-const-mem
        sdl-rw-from-mem

        sdl-rw-from-blob
        sdl-rw-from-string
        sdl-rw-from-u8vector

        sdl-rw-close!)


(define (sdl-rw-from-file path mode)
  (SDL_RWFromFile path mode))

(define (sdl-rw-from-const-mem pointer size)
  (SDL_RWFromConstMem pointer size))

(define (sdl-rw-from-mem pointer size)
  (SDL_RWFromMem pointer size))


(define (sdl-rw-from-blob blob)
  (SDL_RWFromMem
   (make-locative blob) (blob-size blob)))

(define (sdl-rw-from-string str)
  (SDL_RWFromMem
   (make-locative str) (string-length str)))

(define (sdl-rw-from-u8vector u8v)
  (SDL_RWFromMem
   (make-locative u8v) (u8vector-length u8v)))


(define (sdl-rw-close! rwops)
  (let ((response (SDL_RWclose rwops)))
    (%sdl-nullify-struct! rwops)
    response))
