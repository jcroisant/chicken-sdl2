;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export sdl-event-state  sdl-event-state-set!

        sdl-flush-event!
        sdl-flush-events!
        sdl-has-event?
        sdl-has-events?
        sdl-quit-requested?

        sdl-peek-events
        sdl-get-events!

        sdl-poll-event!
        sdl-pump-events!
        sdl-push-event!
        sdl-wait-event!
        sdl-wait-event-timeout!

        sdl-register-events!

        ;; TODO: sdl-add-event-watch!
        ;; TODO: sdl-del-event-watch!
        ;; TODO: sdl-filter-events!
        ;; TODO: sdl-get-event-filter
        ;; TODO: sdl-set-event-filter!

        sdl-get-num-touch-devices
        sdl-get-num-touch-fingers
        sdl-get-touch-device
        sdl-get-touch-finger

        ;; TODO: sdl-record-gesture!
        ;; TODO: sdl-save-dollar-template!
        ;; TODO: sdl-save-all-dollar-templates!
        ;; TODO: sdl-load-dollar-templates
        )


(define (%event-type->int type #!optional fn-name)
  (cond
   ((integer? type)
    type)
   ((symbol? type)
    (sdl-symbol->event-type
     type
     (lambda (t)
       (error fn-name "unrecognized event type" t))))
   (else
    (error fn-name "unrecognized event type" type))))



(define (sdl-event-state-set! type state)
  (SDL_EventState
   (%event-type->int type 'sdl-event-state)
   (if state SDL_ENABLE SDL_DISABLE)))

(define (sdl-event-state type)
  (SDL_EventState
   (%event-type->int type 'sdl-event-state)
   SDL_QUERY))

(set! (setter sdl-event-state)
      sdl-event-state-set!)


(define (sdl-flush-event! type)
  (SDL_FlushEvent
   (%event-type->int type 'sdl-flush-event)))

(define (sdl-flush-events! min-type max-type)
  (SDL_FlushEvents
   (%event-type->int min-type 'sdl-flush-events)
   (%event-type->int max-type 'sdl-flush-events)))

(define (sdl-has-event? type)
  (SDL_HasEvent
   (%event-type->int type 'sdl-has-event?)))

(define (sdl-has-events? min-type max-type)
  (SDL_HasEvents
   (%event-type->int min-type 'sdl-has-events?)
   (%event-type->int max-type 'sdl-has-events?)))

(define (sdl-quit-requested?)
  (SDL_QuitRequested))



;;; Supports the "peek" and "get" actions of SDL_PeepEvents. The "add"
;;; action is not supported because it would almost certainly be less
;;; efficient than calling SDL_PushEvent repeatedly.
(define (%sdl-peep-events num action min-type max-type fn-name)
  (with-temp-mem ((event-array (%sdl-allocate-event-array num)))
    (let ((num-peeped (SDL_PeepEvents
                       event-array num action
                       (%event-type->int min-type fn-name)
                       (%event-type->int max-type fn-name))))
      (%sdl-event-array->list event-array num-peeped))))

(define (sdl-peek-events num #!optional
                         (min-type 'first)
                         (max-type 'last))
  (%sdl-peep-events num SDL_PEEKEVENT
                    min-type max-type
                    'sdl-peek-events))

(define (sdl-get-events! num #!optional
                         (min-type 'first)
                         (max-type 'last))
  (%sdl-peep-events num SDL_GETEVENT
                    min-type max-type
                    'sdl-get-events))



(define (sdl-poll-event! #!optional event-out)
  (let ((ev (or event-out (sdl-alloc-event))))
    (if (SDL_PollEvent ev)
        ev
        #f)))

(define (sdl-pump-events!)
  (SDL_PumpEvents))

(define (sdl-push-event! event)
  (SDL_PushEvent event))

(define (sdl-wait-event! #!optional event-out)
  (let ((ev (or event-out (sdl-alloc-event))))
    (if (SDL_WaitEvent ev)
        ev
        #f)))

;;; NOTE: reverse argument order compared to SDL_WaitEventTimeout.
(define (sdl-wait-event-timeout! timeout-ms #!optional event-out)
  (let ((ev (or event-out (sdl-alloc-event))))
    (if (SDL_WaitEventTimeout ev timeout-ms)
        ev
        #f)))


(define (sdl-register-events! numevents)
  (let ((result (SDL_RegisterEvents numevents)))
    (if (= result %SDL_RegisterEvents-failure-value)
        #f
        result)))


;; TODO: sdl-add-event-watch!
;; TODO: sdl-del-event-watch!
;; TODO: sdl-filter-events!
;; TODO: sdl-get-event-filter
;; TODO: sdl-set-event-filter!


(define (sdl-get-num-touch-devices)
  (SDL_GetNumTouchDevices))

(define (sdl-get-num-touch-fingers touch-id)
  (SDL_GetNumTouchFingers touch-id))

(define (sdl-get-touch-device device-id)
  (SDL_GetTouchDevice device-id))

(define (sdl-get-touch-finger touch-id index)
  (SDL_GetTouchFinger touch-id index))


;; TODO: sdl-record-gesture!
;; TODO: sdl-save-dollar-template!
;; TODO: sdl-save-all-dollar-templates!
;; TODO: sdl-load-dollar-templates
