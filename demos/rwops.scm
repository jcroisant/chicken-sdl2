;; The contents of this demo file are made available under the CC0 1.0
;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This program demonstrates how to create and use sdl-rwops from
;;; memory pointers, blobs, strings, and SRFI-4 u8vectors. This demo
;;; loads and saves BMP image data, but you can also use sdl-rwops for
;;; other things, like loading/saving dollar gestures and loading WAV
;;; audio data. These same techniques also apply to libraries like
;;; SDL2_Image and SDL2_Mixer, which use sdl-rwops to load image and
;;; audio data, respectively.
;;;
;;; It is also possible to create sdl-rwops that access a file, by
;;; passing a file path to sdl-rw-from-file. But, this program does
;;; not demonstrate that use case, because it is so simple.


;;; IMPORTANT: Creating an sdl-rwops from a blob, string, or u8vector
;;; creates a weak reference to the source object's memory. It does
;;; not prevent the source object from being garbage collected. You
;;; should make sure the source object remains in scope for at least
;;; as long as the sdl-rwops. If the source object goes out of scope
;;; and is garbage collected before you use the sdl-rwops, your
;;; program might crash when you try to use the sdl-rwops!
;;;
;;; Two easy ways to prevent that problem are:
;;;
;;; 1. Use and then close the sdl-rwops soon after you create it.
;;;    Don't keep a long-time reference to it.
;;; 2. Or, put the source object in a global definition so it won't be
;;;    garbage collected.


(use sdl2 srfi-4 lolevel miscmacros)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; HARDCODED BMP DATA
;;;
;;; These are data structures each containing the data from a
;;; different 24-bit, 8x8 pixels BMP image file.

;;; A blob with image data of a smiling yellow face.
(define smile-bmp-blob
  '#${424d3a010000000000007a0000006c00000008000000080000000100180000000000c0000000130b0000130b0000000000000000000042475273000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000008000808000800099a60098a60098a60099a680008080008080008000a1ae00a9b500adb800aab600a2ae0098a680008000a4b100b4bd00000000000000000000000000a5b10098a600b1bb00000000d4d900d9df00d4d900c6cd000000009eab00b9c200cfd600e4e800f0f200e4e800d0d600bbc400a4b100bbc300d0d700000000f7f800e7eb00000000bbc500a6b280008000c9cf00d9dd00e1e400dadf00c9d000b6bf80008080008080008000c5cd00cbd200c6cd00bbc4800080800080})

;;; A string with image data of a frowning blue face.
(define frown-bmp-string
  "BM:\x01\x00\x00\x00\x00\x00\x00z\x00\x00\x00l\x00\x00\x00\b\x00\x00\x00\b\x00\x00\x00\x01\x00\x18\x00\x00\x00\x00\x00\xC0\x00\x00\x00\x13\v\x00\x00\x13\v\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00BGRs\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x00\x80\x80\x00\x80\xA6q\x00\xA6q\x00\xA6q\x00\xA6q\x00\x80\x00\x80\x80\x00\x80\x80\x00\x80\xAEw\x00\xB6|\x00\xB8~\x00\xB6|\x00\xAEw\x00\xA6q\x00\x80\x00\x80\xB2y\x00\x00\x00\x00\xC8\x88\x00\xCC\x8B\x00\xC8\x88\x00\xC0\x83\x00\x00\x00\x00\xA6q\x00\xBC\x80\x00\xCC\x8B\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xBE\x82\x00\xACu\x00\xC2\x84\x00\xD6\x92\x00\xE8\x9E\x00\xF2\xA5\x00\xE8\x9E\x00\xD6\x92\x00\xC4\x86\x00\xB2y\x00\xC4\x86\x00\xD8\x93\x00\x00\x00\x00\xF8\xA9\x00\xEC\xA1\x00\x00\x00\x00\xC6\x87\x00\xB2y\x00\x80\x00\x80\xD0\x8E\x00\xDE\x97\x00\xE4\x9C\x00\xE0\x99\x00\xD0\x8E\x00\xC0\x83\x00\x80\x00\x80\x80\x00\x80\x80\x00\x80\xCE\x8D\x00\xD2\x8F\x00\xCE\x8D\x00\xC4\x86\x00\x80\x00\x80\x80\x00\x80")

;;; A SRFI-4 u8vector with image data of a green face with a tongue.
(define tongue-bmp-u8vector
  '#u8(66 77 138 1 0 0 0 0 0 0 138 0 0 0 124 0 0 0 8 0 0 0 8 0 0 0 1 0 32 0 3 0 0 0 0 1 0 0 19 11 0 0 19 11 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 66 71 82 115 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 128 0 128 255 128 0 128 255 0 166 0 255 0 166 0 255 0 166 0 255 0 166 0 255 128 0 128 255 128 0 128 255 128 0 128 255 0 174 0 255 0 182 0 255 0 184 0 255 0 0 189 255 0 0 189 255 0 166 0 255 128 0 128 255 0 178 0 255 0 190 0 255 0 200 0 255 0 204 0 255 0 0 189 255 0 0 189 255 0 178 0 255 0 166 0 255 0 188 0 255 0 204 0 255 0 0 0 255 0 0 0 255 0 0 0 255 0 0 0 255 0 190 0 255 0 172 0 255 0 194 0 255 0 214 0 255 0 232 0 255 0 242 0 255 0 232 0 255 0 214 0 255 0 196 0 255 0 178 0 255 0 196 0 255 0 216 0 255 0 0 0 255 0 248 0 255 0 236 0 255 0 0 0 255 0 198 0 255 0 178 0 255 128 0 128 255 0 208 0 255 0 222 0 255 0 228 0 255 0 224 0 255 0 208 0 255 0 192 0 255 128 0 128 255 128 0 128 255 128 0 128 255 0 206 0 255 0 210 0 255 0 206 0 255 0 196 0 255 128 0 128 255 128 0 128))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOADING IMAGE DATA USING RWOPS

;;; An sdl-surface loaded from smile-bmp-blob.
(define smile-surf
  (sdl-load-bmp-rw
   ;; Create a sdl-rwops from blob
   (sdl-rw-from-blob smile-bmp-blob)
   ;; close the rwops after loading
   #t))

;;; An sdl-surface loaded from frown-bmp-string.
(define frown-surf
  (sdl-load-bmp-rw
   ;; Create a sdl-rwops from string
   (sdl-rw-from-string frown-bmp-string)
   ;; close the rwops after loading
   #t))

;;; An sdl-surface loaded from tongue-bmp-u8vector.
(define tongue-surf
  (sdl-load-bmp-rw
   ;; Create a sdl-rwops from u8vector
   (sdl-rw-from-u8vector tongue-bmp-u8vector)
   ;; close the rwops after loading
   #t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BLIT THE FACES TOGETHER

;;; A 24x8 24-bpp surface, to hold the three faces side by side.
(define faces-surf (sdl-make-surface 24 8 24))

(sdl-blit-surface! smile-surf  #f faces-surf (sdl-make-rect 0 0))
(sdl-blit-surface! frown-surf  #f faces-surf (sdl-make-rect 8 0))
(sdl-blit-surface! tongue-surf #f faces-surf (sdl-make-rect 16 0))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SAVE IMAGE DATA TO RWOPS

;;; Now allocate a block of memory and save faces-surf as a BMP image
;;; data into that pointer, to show it's possible to save to a rwops.
;;; Instead of a pointer you could also do this with a blob, using
;;; make-blob instead of allocate.

;;; Calculate how many bytes of memory to allocate to hold the BMP
;;; data. 512 bytes is a safe estimate about how much extra BMP data
;;; there might be. It's safe to allocate more memory than you need,
;;; but sdl-save-bmp-rw! will fail if you allocate not enough memory.
(define faces-bmp-pointer-size
  (+ 512  ;; extra room for BMP headers, etc.
     (* 3 ;; 3 bytes (24 bits) per pixel
        (sdl-surface-pitch faces-surf)
        (sdl-surface-h faces-surf))))

(define faces-bmp-pointer
  (allocate faces-bmp-pointer-size))


(define (save-bmp-to-pointer! surf pointer size)
  (sdl-save-bmp-rw!
   surf
   ;; Create a sdl-rwops from a pointer and size (bytes). Because this
   ;; sdl-rwops will be written to, you must use sdl-rw-from-mem.
   ;; Below we show how to create a read-only memory sdl-rwops.
   (sdl-rw-from-mem pointer size)
   ;; close the rwops after saving
   #t))


;;; Save faces-surf to the pointer.
(save-bmp-to-pointer! faces-surf
                      faces-bmp-pointer
                      faces-bmp-pointer-size)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RELOAD IMAGE DATA

;;; Now load the BMP data that was just saved, to create another
;;; surface with the same contents as faces-surf.


(define (load-bmp-from-pointer pointer size)
  (sdl-load-bmp-rw
   ;; Create a sdl-rwops from a pointer and size (bytes). Because the
   ;; pointer will only be read from (not written to), you can create
   ;; a read-only sdl-rwops using sdl-rw-from-const-mem. It would be
   ;; perfectly fine to use sdl-rw-from-mem here too, but since this
   ;; is a demo we will show how to do a read-only sdl-rwops.
   (sdl-rw-from-const-mem pointer size)
   ;; close the rwops after loading
   #t))


(define faces2-surf
  (load-bmp-from-pointer
   faces-bmp-pointer
   faces-bmp-pointer-size))


;;; Free faces-bmp-pointer's memory now that we are done with it. This
;;; is not really necessary in this program because the program will
;;; end soon, but in long-running programs it is important to free
;;; memory that you allocate, once you are done using the memory.
(free faces-bmp-pointer)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DISPLAY THE RESULTS

;;; Now open a window and blit faces-surf (on top) and faces2-surf
;;; (below), so the user can see that everything worked correctly. The
;;; surfaces are quite small (24x8 each), so do a scaled blit to make
;;; them bigger so the user can see them better.

(define window (sdl-create-window!
                "RWops Demo"
                'undefined 'undefined
                384 256))

(define window-surf (sdl-window-surface window))

(sdl-blit-scaled! faces-surf #f window-surf
                  (sdl-make-rect 0 0 384 128))

(sdl-blit-scaled! faces2-surf #f window-surf
                  (sdl-make-rect 0 128 384 128))

(sdl-update-window-surface! window)


;;; Wait for the user to close the window.
(while (not (sdl-quit-requested?))
  (sdl-wait-event!))
