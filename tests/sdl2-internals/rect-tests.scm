
(test-begin "rect")


(test-group "sdl-make-rect"
  (test-assert (sdl-rect? (sdl-make-rect)))
  (test 0 (sdl-rect-x (sdl-make-rect)))
  (test 0 (sdl-rect-y (sdl-make-rect)))
  (test 0 (sdl-rect-w (sdl-make-rect)))
  (test 0 (sdl-rect-h (sdl-make-rect)))

  (test-assert (sdl-rect? (sdl-make-rect 1)))
  (test 1 (sdl-rect-x (sdl-make-rect 1)))
  (test 0 (sdl-rect-y (sdl-make-rect 1)))
  (test 0 (sdl-rect-w (sdl-make-rect 1)))
  (test 0 (sdl-rect-h (sdl-make-rect 1)))

  (test-assert (sdl-rect? (sdl-make-rect 1 2 3 4)))
  (test 1 (sdl-rect-x (sdl-make-rect 1 2 3 4)))
  (test 2 (sdl-rect-y (sdl-make-rect 1 2 3 4)))
  (test 3 (sdl-rect-w (sdl-make-rect 1 2 3 4)))
  (test 4 (sdl-rect-h (sdl-make-rect 1 2 3 4))))


(test-group "sdl-rect?"
  (test-assert (sdl-rect? (sdl-make-rect)))
  (test-assert (sdl-rect? (sdl-make-rect 1 2 3 4)))
  (test-assert (not (sdl-rect? '(1 2 3 4))))
  (test-assert (not (sdl-rect? #(1 2 3 4))))
  (test-assert (not (sdl-rect? (sdl-make-point)))))


(test-integer-struct-fields
 make: (sdl-make-rect)
 freer: sdl-free-rect!
 (x
  getter: sdl-rect-x
  setter: sdl-rect-x-set!
  min: Sint32-min
  max: Sint32-max)
 (y
  getter: sdl-rect-y
  setter: sdl-rect-y-set!
  min: Sint32-min
  max: Sint32-max)
 (w
  getter: sdl-rect-w
  setter: sdl-rect-w-set!
  min: Sint32-min
  max: Sint32-max)
 (h
  getter: sdl-rect-h
  setter: sdl-rect-h-set!
  min: Sint32-min
  max: Sint32-max))


(test-group "sdl-rect-set!"
  (let ((rect (sdl-make-rect)))
    (test-assert "returns the rect"
                 (eq? rect (sdl-rect-set! rect 5 6 7 8))))
  (test "sets all fields if all values are specified"
        '(5 6 7 8)
        (sdl-rect->list
         (sdl-rect-set! (sdl-make-rect 1 2 3 4) 5 6 7 8)))
  (test "does not change fields where the value is omitted"
        '(5 6 3 4)
        (sdl-rect->list
         (sdl-rect-set! (sdl-make-rect 1 2 3 4) 5 6)))
  (test "has no effect if all values are omitted"
        '(1 2 3 4)
        (sdl-rect->list
         (sdl-rect-set! (sdl-make-rect 1 2 3 4))))
  (test "does not change fields where the value is #f"
        '(1 8 3 9)
        (sdl-rect->list
         (sdl-rect-set! (sdl-make-rect 1 2 3 4) #f 8 #f 9)))
  (test "has no effect if all values are #f"
        '(1 2 3 4)
        (sdl-rect->list
         (sdl-rect-set! (sdl-make-rect 1 2 3 4) #f #f #f #f))))


(test-group "sdl-alloc-rect"
  (let ((rect (sdl-alloc-rect)))
    (test-assert (sdl-rect? rect))
    (test-assert (integer? (sdl-rect-x rect)))
    (test-assert (integer? (sdl-rect-y rect)))
    (test-assert (integer? (sdl-rect-w rect)))
    (test-assert (integer? (sdl-rect-h rect)))))

(test-group "sdl-alloc-rect*"
  (let ((rect (sdl-alloc-rect*)))
    (test-assert (sdl-rect? rect))
    (test-assert (integer? (sdl-rect-x rect)))
    (test-assert (integer? (sdl-rect-y rect)))
    (test-assert (integer? (sdl-rect-w rect)))
    (test-assert (integer? (sdl-rect-h rect)))
    (sdl-free-rect! rect)))


(test-group "sdl-free-rect!"
  (let ((rect (sdl-make-rect)))
    (sdl-free-rect! rect)
    (test-assert "sets the record's pointer to null"
                 (sdl-struct-null? rect)))

  (let ((rect (sdl-make-rect)))
    (test-assert "returns the same instance"
                 (eq? rect (sdl-free-rect! rect)))
    (test-assert "is safe to use multiple times on the same rect"
                 (eq? rect (sdl-free-rect! rect))))

  (test-error (sdl-free-rect! 0))
  (test-error (sdl-free-rect! #f))
  (test-error (sdl-free-rect! '(1 2 3 4)))
  (test-error (sdl-free-rect! (sdl-make-point))))


(test-end "rect")
