
(test-begin "version")


(test-group "sdl-make-version"
  (test-assert (sdl-version? (sdl-make-version)))
  (test 0 (sdl-version-major (sdl-make-version)))
  (test 0 (sdl-version-minor (sdl-make-version)))
  (test 0 (sdl-version-patch (sdl-make-version)))

  (test-assert (sdl-version? (sdl-make-version 1)))
  (test 1 (sdl-version-major (sdl-make-version 1)))
  (test 0 (sdl-version-minor (sdl-make-version 1)))
  (test 0 (sdl-version-patch (sdl-make-version 1)))

  (test-assert (sdl-version? (sdl-make-version 1 2 3)))
  (test 1 (sdl-version-major (sdl-make-version 1 2 3)))
  (test 2 (sdl-version-minor (sdl-make-version 1 2 3)))
  (test 3 (sdl-version-patch (sdl-make-version 1 2 3))))


(test-group "sdl-version?"
  (test-assert (sdl-version? (sdl-make-version)))
  (test-assert (sdl-version? (sdl-make-version 1 2 3)))
  (test-assert (not (sdl-version? '(1 2 3))))
  (test-assert (not (sdl-version? #(1 2 3))))
  (test-assert (not (sdl-version? (sdl-make-point)))))


(test-integer-struct-fields
 make: (sdl-make-version)
 freer: sdl-free-version!
 (major
  getter: sdl-version-major
  setter: sdl-version-major-set!
  min: Uint8-min
  max: Uint8-max)
 (minor
  getter: sdl-version-minor
  setter: sdl-version-minor-set!
  min: Uint8-min
  max: Uint8-max)
 (patch
  getter: sdl-version-patch
  setter: sdl-version-patch-set!
  min: Uint8-min
  max: Uint8-max))


(test-group "sdl-version-set!"
  (let ((version (sdl-make-version)))
    (test-assert "returns the version"
                 (eq? version (sdl-version-set! version 5 6 7))))
  (test "sets all fields if all values are specified"
        '(5 6 7)
        (sdl-version->list
         (sdl-version-set! (sdl-make-version 1 2 3) 5 6 7)))
  (test "does not change fields where the value is omitted"
        '(5 6 3)
        (sdl-version->list
         (sdl-version-set! (sdl-make-version 1 2 3) 5 6)))
  (test "has no effect if all values are omitted"
        '(1 2 3)
        (sdl-version->list
         (sdl-version-set! (sdl-make-version 1 2 3))))
  (test "does not change fields where the value is #f"
        '(1 2 8)
        (sdl-version->list
         (sdl-version-set! (sdl-make-version 1 2 3) #f #f 8)))
  (test "has no effect if all values are #f"
        '(1 2 3)
        (sdl-version->list
         (sdl-version-set! (sdl-make-version 1 2 3) #f #f #f))))


(test-group "sdl-alloc-version"
  (let ((version (sdl-alloc-version)))
    (test-assert (sdl-version? version))
    (test-assert (integer? (sdl-version-major version)))
    (test-assert (integer? (sdl-version-minor version)))
    (test-assert (integer? (sdl-version-patch version)))))

(test-group "sdl-alloc-version*"
  (let ((version (sdl-alloc-version*)))
    (test-assert (sdl-version? version))
    (test-assert (integer? (sdl-version-major version)))
    (test-assert (integer? (sdl-version-minor version)))
    (test-assert (integer? (sdl-version-patch version)))
    (sdl-free-version! version)))


(test-group "sdl-free-version!"
  (let ((version (sdl-make-version)))
    (sdl-free-version! version)
    (test-assert "sets the record's pointer to null"
                 (sdl-struct-null? version)))

  (let ((version (sdl-make-version)))
    (test-assert "returns the same instance"
                 (eq? version (sdl-free-version! version)))
    (test-assert "is safe to use multiple times on the same version"
                 (eq? version (sdl-free-version! version))))

  (test-error (sdl-free-version! 0))
  (test-error (sdl-free-version! #f))
  (test-error (sdl-free-version! '(1 2 3)))
  (test-error (sdl-free-version! (sdl-make-point))))


(test-end "version")
