
(test-begin "keysym")


(test-enum-field
 make: (sdl-alloc-keysym)
 getter: (sdl-keysym-scancode
          sdl-keysym-scancode-raw)
 setter: (sdl-keysym-scancode-set!
          sdl-keysym-scancode-raw-set!)
 valid1: ('lang1
          SDL_SCANCODE_LANG1)
 valid2: ('app1
          SDL_SCANCODE_APP1)
 invalid: ('foobar
           1234))


(test-enum-field
 make: (sdl-alloc-keysym)
 getter: (sdl-keysym-sym
          sdl-keysym-sym-raw)
 setter: (sdl-keysym-sym-set!
          sdl-keysym-sym-raw-set!)
 valid1: ('quote-dbl
          SDLK_QUOTEDBL)
 valid2: ('at
          SDLK_AT)
 invalid: ('foobar
           1234))


(test-group "sdl-keysym-mod / sdl-keysym-mod-set!"
  (let ((record (sdl-alloc-keysym)))
    (sdl-keysym-mod-raw-set! record 0)
    (test "Getter returns empty list if no masks match"
          '() (sdl-keysym-mod record))

    (sdl-keysym-mod-raw-set!
     record
     (bitwise-ior KMOD_LSHIFT KMOD_RALT KMOD_CAPS))
    (test "Getter returns list of expected symbols"
          (sort-symbols '(shift lshift alt ralt caps))
          (sort-symbols (sdl-keysym-mod record)))

    (test "Getter optional arg controls bitmask match exactness"
          (sort-symbols '(lshift ralt caps))
          (sort-symbols (sdl-keysym-mod record #t)))

    (sdl-keysym-mod-set! record '(alt shift))
    (test "Setting to list of recognized symbols works"
          (sort-symbols '(alt lalt ralt shift rshift lshift))
          (sort-symbols (sdl-keysym-mod record)))

    (sdl-keysym-mod-set! record (bitwise-ior KMOD_RGUI KMOD_NUM))
    (test "Setting to integer works"
          (sort-symbols '(gui rgui num))
          (sort-symbols (sdl-keysym-mod record)))

    (test-error "Throws error if set to invalid type (symbol)"
                (sdl-keysym-mod-set! record 'gui))
    (test-error "Throws error if set to invalid type (float)"
                (sdl-keysym-mod-set! record 1.23))

    (test-error "Throws error if set to list containing unrecognized symbol"
                (sdl-keysym-mod-set! record '(alt foo gui)))
    (test-error "Throws error if set to list containing integer"
                (sdl-keysym-mod-set! record (list 'alt KMOD_GUI)))))


(test-end "keysym")
