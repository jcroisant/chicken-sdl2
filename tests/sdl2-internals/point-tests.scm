
(test-begin "point")


(test-group "sdl-make-point"
  (test-assert (sdl-point? (sdl-make-point)))
  (test 0 (sdl-point-x (sdl-make-point)))
  (test 0 (sdl-point-y (sdl-make-point)))

  (test-assert (sdl-point? (sdl-make-point 1)))
  (test 1 (sdl-point-x (sdl-make-point 1)))
  (test 0 (sdl-point-y (sdl-make-point 1)))

  (test-assert (sdl-point? (sdl-make-point 1 2)))
  (test 1 (sdl-point-x (sdl-make-point 1 2)))
  (test 2 (sdl-point-y (sdl-make-point 1 2))))


(test-group "sdl-point?"
  (test-assert (sdl-point? (sdl-make-point)))
  (test-assert (sdl-point? (sdl-make-point 1 2)))
  (test-assert (not (sdl-point? '(1 2))))
  (test-assert (not (sdl-point? #(1 2))))
  (test-assert (not (sdl-point? (sdl-make-rect)))))


(test-integer-struct-fields
 make: (sdl-make-point)
 freer: sdl-free-point!
 (x
  getter: sdl-point-x
  setter: sdl-point-x-set!
  min: Sint32-min
  max: Sint32-max)
 (y
  getter: sdl-point-y
  setter: sdl-point-y-set!
  min: Sint32-min
  max: Sint32-max))


(test-group "sdl-point-set!"
  (let ((point (sdl-make-point)))
    (test-assert "returns the point"
                 (eq? point (sdl-point-set! point 3 4))))
  (test "sets all fields if all values are specified"
        '(3 4)
        (sdl-point->list
         (sdl-point-set! (sdl-make-point 1 2) 3 4)))
  (test "does not change fields where the value is omitted"
        '(3 2)
        (sdl-point->list
         (sdl-point-set! (sdl-make-point 1 2) 3)))
  (test "has no effect if all values are omitted"
        '(1 2)
        (sdl-point->list
         (sdl-point-set! (sdl-make-point 1 2))))
  (test "does not change fields where the value is #f"
        '(1 4)
        (sdl-point->list
         (sdl-point-set! (sdl-make-point 1 2) #f 4)))
  (test "has no effect if all values are #f"
        '(1 2)
        (sdl-point->list
         (sdl-point-set! (sdl-make-point 1 2) #f #f))))


(test-group "sdl-alloc-point"
  (let ((point (sdl-alloc-point)))
    (test-assert (sdl-point? point))
    (test-assert (integer? (sdl-point-x point)))
    (test-assert (integer? (sdl-point-y point)))))

(test-group "sdl-alloc-point*"
  (let ((point (sdl-alloc-point*)))
    (test-assert (sdl-point? point))
    (test-assert (integer? (sdl-point-x point)))
    (test-assert (integer? (sdl-point-y point)))
    (sdl-free-point! point)))


(test-group "sdl-free-point!"
  (let ((point (sdl-make-point)))
    (sdl-free-point! point)
    (test-assert "sets the record's pointer to null"
                 (sdl-struct-null? point)))

  (let ((point (sdl-make-point)))
    (test-assert "returns the same instance"
                 (eq? point (sdl-free-point! point)))
    (test-assert "is safe to use multiple times on the same point"
                 (eq? point (sdl-free-point! point))))

  (test-error (sdl-free-point! 0))
  (test-error (sdl-free-point! #f))
  (test-error (sdl-free-point! '(1 2)))
  (test-error (sdl-free-point! (sdl-make-rect))))


(test-end "point")
