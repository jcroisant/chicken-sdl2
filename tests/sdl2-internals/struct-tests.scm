(test-group "sdl-struct-null?"
  (test-group "with sdl-color"
    (test "returns #t if record pointer is null"
          #t
          (sdl-struct-null?
           (%sdl-wrap-color (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f (sdl-struct-null? (sdl-make-color))))

  (test-group "with sdl-event"
    (test "returns #t if record pointer is null"
          #t
          (sdl-struct-null?
           (%sdl-wrap-event (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl-struct-null? (sdl-alloc-event))))

  (test-group "with sdl-point"
    (test "returns #t if record pointer is null"
          #t
          (sdl-struct-null?
           (%sdl-wrap-point (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl-struct-null? (sdl-make-point))))

  (test-group "with sdl-rect"
    (test "returns #t if record pointer is null"
          #t
          (sdl-struct-null?
           (%sdl-wrap-rect (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl-struct-null? (sdl-make-rect))))

  (test-group "with sdl-surface"
    (test "returns #t if record pointer is null"
          #t
          (sdl-struct-null?
           (%sdl-wrap-surface (address->pointer 0))))
    (let ((surface (SDL_CreateRGBSurface 0 1 1 16 0 0 0 0)))
      (test "returns #f if record pointer is non-null"
            #f
            (sdl-struct-null? surface))
      (SDL_FreeSurface surface)))

  (test-group "with sdl-version"
    (test "returns #t if record pointer is null"
          #t
          (sdl-struct-null?
           (%sdl-wrap-version (address->pointer 0))))
    (test "returns #f if record pointer is non-null"
          #f
          (sdl-struct-null? (sdl-make-version))))

  (test-group "with invalid types"
    (test-error "throws error if given a non-null pointer"
                (sdl-struct-null?
                 (%unwrap-sdl-rect (sdl-make-rect))))
    (test-error "throws error if given a null pointer"
                (sdl-struct-null? (address->pointer 0)))
    (test-error "throws error if given #f"
                (sdl-struct-null? #f))
    (test-error "throws error if given a number"
                (sdl-struct-null? 0))))
