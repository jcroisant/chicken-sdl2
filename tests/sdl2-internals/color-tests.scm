
(test-begin "color")


(test-group "sdl-make-color"
  (test-assert (sdl-color? (sdl-make-color)))
  (test 0 (sdl-color-r (sdl-make-color)))
  (test 0 (sdl-color-g (sdl-make-color)))
  (test 0 (sdl-color-b (sdl-make-color)))
  (test 255 (sdl-color-a (sdl-make-color)))

  (test-assert (sdl-color? (sdl-make-color 1)))
  (test 1 (sdl-color-r (sdl-make-color 1)))
  (test 0 (sdl-color-g (sdl-make-color 1)))
  (test 0 (sdl-color-b (sdl-make-color 1)))
  (test 255 (sdl-color-a (sdl-make-color 1)))

  (test-assert (sdl-color? (sdl-make-color 1 2 3 4)))
  (test 1 (sdl-color-r (sdl-make-color 1 2 3 4)))
  (test 2 (sdl-color-g (sdl-make-color 1 2 3 4)))
  (test 3 (sdl-color-b (sdl-make-color 1 2 3 4)))
  (test 4 (sdl-color-a (sdl-make-color 1 2 3 4))))


(test-group "sdl-color?"
  (test-assert (sdl-color? (sdl-make-color)))
  (test-assert (sdl-color? (sdl-make-color 1 2 3 4)))
  (test-assert (not (sdl-color? '(1 2 3 4))))
  (test-assert (not (sdl-color? #(1 2 3 4))))
  (test-assert (not (sdl-color? (sdl-make-point)))))


(test-integer-struct-fields
 make: (sdl-make-color)
 freer: sdl-free-color!
 (x
  getter: sdl-color-r
  setter: sdl-color-r-set!
  min: Uint8-min
  max: Uint8-max)
 (y
  getter: sdl-color-g
  setter: sdl-color-g-set!
  min: Uint8-min
  max: Uint8-max)
 (w
  getter: sdl-color-b
  setter: sdl-color-b-set!
  min: Uint8-min
  max: Uint8-max)
 (h
  getter: sdl-color-a
  setter: sdl-color-a-set!
  min: Uint8-min
  max: Uint8-max))


(test-group "sdl-color-set!"
  (let ((color (sdl-make-color)))
    (test-assert "returns the color"
                 (eq? color (sdl-color-set! color 5 6 7 8))))
  (test "sets all fields if all values are specified"
        '(5 6 7 8)
        (sdl-color->list
         (sdl-color-set! (sdl-make-color 1 2 3 4) 5 6 7 8)))
  (test "does not change fields where the value is omitted"
        '(5 6 3 4)
        (sdl-color->list
         (sdl-color-set! (sdl-make-color 1 2 3 4) 5 6)))
  (test "has no effect if all values are omitted"
        '(1 2 3 4)
        (sdl-color->list
         (sdl-color-set! (sdl-make-color 1 2 3 4))))
  (test "does not change fields where the value is #f"
        '(1 8 3 9)
        (sdl-color->list
         (sdl-color-set! (sdl-make-color 1 2 3 4) #f 8 #f 9)))
  (test "has no effect if all values are #f"
        '(1 2 3 4)
        (sdl-color->list
         (sdl-color-set! (sdl-make-color 1 2 3 4) #f #f #f #f))))


(test-group "sdl-alloc-color"
  (let ((color (sdl-alloc-color)))
    (test-assert (sdl-color? color))
    (test-assert (integer? (sdl-color-r color)))
    (test-assert (integer? (sdl-color-g color)))
    (test-assert (integer? (sdl-color-b color)))
    (test-assert (integer? (sdl-color-a color)))))

(test-group "sdl-alloc-color*"
  (let ((color (sdl-alloc-color*)))
    (test-assert (sdl-color? color))
    (test-assert (integer? (sdl-color-r color)))
    (test-assert (integer? (sdl-color-g color)))
    (test-assert (integer? (sdl-color-b color)))
    (test-assert (integer? (sdl-color-a color)))
    (sdl-free-color! color)))


(test-group "sdl-free-color!"
  (let ((color (sdl-make-color)))
    (sdl-free-color! color)
    (test-assert "sets the record's pointer to null"
                 (sdl-struct-null? color)))

  (let ((color (sdl-make-color)))
    (test-assert "returns the same instance"
                 (eq? color (sdl-free-color! color)))
    (test-assert "is safe to use multiple times on the same color"
                 (eq? color (sdl-free-color! color))))

  (test-error (sdl-free-color! 0))
  (test-error (sdl-free-color! #f))
  (test-error (sdl-free-color! '(1 2 3 4)))
  (test-error (sdl-free-color! (sdl-make-point))))


(test-group "aliases"
  (test-assert (eq? sdl-colour?       sdl-color?))
  (test-assert (eq? sdl-free-colour!  sdl-free-color!))
  (test-assert (eq? sdl-alloc-colour* sdl-alloc-color*))
  (test-assert (eq? sdl-alloc-colour  sdl-alloc-color))
  (test-assert (eq? sdl-colour-r      sdl-color-r))
  (test-assert (eq? sdl-colour-g      sdl-color-g))
  (test-assert (eq? sdl-colour-b      sdl-color-b))
  (test-assert (eq? sdl-colour-a      sdl-color-a))
  (test-assert (eq? sdl-colour-r-set! sdl-color-r-set!))
  (test-assert (eq? sdl-colour-g-set! sdl-color-g-set!))
  (test-assert (eq? sdl-colour-b-set! sdl-color-b-set!))
  (test-assert (eq? sdl-colour-a-set! sdl-color-a-set!))
  (test-assert (eq? sdl-make-colour   sdl-make-color))
  (test-assert (eq? sdl-colour-set!   sdl-color-set!))
  (test-assert (eq? sdl-colour->list  sdl-color->list))

  (let ((colour (sdl-make-colour 1 2 3 4)))
    (set! (sdl-colour-r colour) 5)
    (set! (sdl-colour-g colour) 6)
    (set! (sdl-colour-b colour) 7)
    (set! (sdl-colour-a colour) 8)
    (test '(5 6 7 8) (sdl-colour->list colour))))


(test-end "color")
