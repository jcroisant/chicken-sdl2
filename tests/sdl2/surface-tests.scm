
(test-begin "surface")


(test-group "sdl-free-surface!"
  (let ((surface (SDL_CreateRGBSurface 0 10 10 24 0 0 0 0)))
    (assert (not (sdl-struct-null? surface)))
    (test "returns void"
          (void) (sdl-free-surface! surface))
    (test-assert "nullifies the surface"
                 (sdl-struct-null? surface)))

  (test-error "throws error if given non-surface"
              (sdl-free-surface! (sdl-make-rect))))



(test-group "sdl-make-surface"
  (define test-make-surface-with-valid-depth
    (lambda (depth)
      (test-group (sprintf "depth ~A" depth)
        (let ((surface (sdl-make-surface 10 20 depth)))
          (test-assert "creates and returns a new sdl-surface"
            (and (sdl-surface? surface)
                 (not (sdl-struct-null? surface))))

          (test "surface has the correct width and height"
                '(10 20) (list (sdl-surface-w surface)
                               (sdl-surface-h surface)))

          (let ((format (sdl-surface-format surface)))
            (test "surface has the correct depth"
                  depth (sdl-pixel-format-bits-per-pixel format))

            (when (<= depth 8)
              (test "surface has all zero masks"
                    (list 0 0 0 0)
                    (list (sdl-pixel-format-rmask format)
                          (sdl-pixel-format-gmask format)
                          (sdl-pixel-format-bmask format)
                          (sdl-pixel-format-amask format)))
              (test-assert "surface has a palette"
                (not (sdl-struct-null?
                      (sdl-pixel-format-palette format)))))

            (when (<= 15 depth 24)
              (test-assert "surface has non-zero Rmask"
                (positive? (sdl-pixel-format-rmask format)))
              (test-assert "surface has non-zero Gmask"
                (positive? (sdl-pixel-format-gmask format)))
              (test-assert "surface has non-zero Bmask"
                (positive? (sdl-pixel-format-bmask format)))
              (test-assert "surface has zero Amask"
                (zero? (sdl-pixel-format-amask format))))

            (when (= depth 32)
              (test "surface has appropriate masks for system byte order"
                    (if (= SDL_BYTEORDER SDL_BIG_ENDIAN)
                        (list #xff000000
                              #x00ff0000
                              #x0000ff00
                              #x000000ff)
                        (list #x000000ff
                              #x0000ff00
                              #x00ff0000
                              #xff000000))
                    (list (sdl-pixel-format-rmask format)
                          (sdl-pixel-format-gmask format)
                          (sdl-pixel-format-bmask format)
                          (sdl-pixel-format-amask format)))))))))

  (for-each test-make-surface-with-valid-depth
            '(1 4 8 12 15 16 24 32))

  (test-error "throws error if width is not an integer"
    (sdl-make-surface 10.5 20 32))
  (test-error "throws error if width is negative"
    (sdl-make-surface -1 20 32))

  (test-error "throws error if height is not an integer"
    (sdl-make-surface 10 20.5 32))
  (test-error "throws error if height is negative"
    (sdl-make-surface 10 -1 32))

  (test-error "throws error if depth is not an integer"
    (sdl-make-surface 10 20 24.5))
  (test-error "throws error if depth is negative"
    (sdl-make-surface 10 20 -24)))



(test-end "surface")
